#!/usr/bin/env python

import argparse
from libs.MainApplication import MainApplication


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug-pi", action="store_true", default=False)
    parser.add_argument("--debug-camera", action="store_true", default=False)
    parser.add_argument("--pro-mode", action="store_true", default=False)
    args = parser.parse_args()
    MainApplication(args.debug_camera, args.debug_pi, args.pro_mode)
