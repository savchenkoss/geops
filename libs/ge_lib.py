import os

from ctypes import c_int
from ctypes import c_char_p
from ctypes import POINTER
from ctypes import c_bool
from ctypes import c_float
from ctypes import c_void_p
from ctypes import byref
if os.name == "nt":
    from ctypes import windll
else:
    from ctypes import CDLL


c_int_p = POINTER(c_int)

# Define constants
# 1) Message constants
messages = {
    # camera detected and ok
    0: "Camera_Ok",
    # no camera detected
    1: "NoCamera_Connected",
    # there is a problem with the USB interface
    2: "could_not_open_USBDevice ",
    # transferring data to cam failed - TimeOut!
    3: "WriteConfigTable_Failed",
    # receiving data  from cam failed - TimeOut!
    4: "WriteReadRequest_Failed",
    # no extern trigger signal within time window of TriggerTimeOut
    5: "NoTrigger",
    # new cam detected - you need to perform CamSettings
    6: "NewCameraDetected",
    # this DLL was not written for connected cam - please request new greateyes.dll
    7: "UnknownCamID",
    # one ore more parameters are out of range
    8: "OutofRange",
    # no new image data
    9: "NoNewData",
    # camera busy
    10: "Busy",
    # cooling turned off
    11: "CoolingTurnedOff",
    # measurement stopped
    12: "MeasurementStopped",
    # too many pixels for BurstMode. Set lower number of measurements or higher binning level
    13: "BurstModeTooMuchPixels",
    # timing table for selected readout speed not found
    14: "TimingTableNotFound",
    # function stopped but there is no critical error (no valid result;
    # catched division by zero). please try to call function again.
    15: "NotCritical",
    # for firmware < v12 the combination of binning and crop mode is not supported
    16: "IllegalCombinationBinCrop"
}
#  1.2 Function Constants
maxPixelBurstTransfer = c_int(8823794)
sensorFeature_capacityMode = c_int(0)
sensorFeature_cropX = c_int(1)
sensorFeature_binningX = c_int(2)
readout_speeds = {
    "50_kHz": c_int(50),
    "100_kHz": c_int(100),
    "250_kHz": c_int(250),
    "500_kHz": c_int(500),
    "1_MHz": c_int(1000),
    "3_MHz": c_int(3000),
    "5_MHz": c_int(5000)}
connectionType_USB = c_int(0)
connectionType_Ethernet = c_int(3)


# Define all function prototypes to set their argtypes and restypes
# when the DLL is loaded
_API_FUNCTION_PROTOTYPES = [
    # Connect/disconnect related functions
    ("SetupCameraInterface", [c_int, c_char_p, c_int_p, c_int], c_bool),
    ("ConnectToSingleCameraServer", [c_int], c_bool),
    ("ConnectToMultiCameraServer", [], c_bool),
    ("DisconnectCameraServer", [c_int], c_bool),
    ("GetNumberOfConnectedCams", [], c_int),
    ("ConnectCamera", [c_int_p, POINTER(c_char_p), c_int_p, c_int], c_bool),
    ("DisconnectCamera", [c_int_p, c_int], c_bool),
    ("InitCamera", [c_int_p, c_int], c_bool),
    ("SetExposure", [c_int, c_int_p, c_int], c_bool),
    ("SetReadOutSpeed", [c_int, c_int_p, c_int], c_bool),
    ("SetBinningMode", [c_int, c_int, c_int_p, c_int], c_bool),
    ("SetShutterTimings", [c_int, c_int, c_int_p, c_int], c_bool),
    ("OpenShutter", [c_int, c_int_p, c_int], c_bool),
    ("SyncOutput", [c_bool, c_int_p, c_int], c_bool),
    ("SetupBurstMode", [c_int, c_int_p, c_int], c_bool),
    ("ActivateBurstMode", [c_bool, c_int_p, c_int], c_int),
    ("SetupCropMode2D", [c_int, c_int, c_int_p, c_int], c_bool),
    ("ActivateCropMode", [c_bool, c_int_p, c_int], c_int),
    ("SetupGain", [c_int, c_int_p, c_int], c_bool),
    ("SetupCapacityMode", [c_bool, c_int_p, c_int], c_bool),
    ("SetupTransferOptions", [c_bool, c_bool], c_bool),
    ("SetupSensorOutputMode", [c_int, c_int], c_bool),
    ("ClearFifo", [c_int_p, c_int], c_int),
    ("SetBitDepth", [c_int, c_int_p, c_int], c_bool),
    ("SetExtTriggerTimeOut", [c_int, c_int], c_bool),
    ("SetBusyTimeout", [c_int], c_bool),
    ("SetLEDStatus", [c_bool, c_int_p, c_int], c_bool),
    ("GetDLLVersion", [c_int_p], c_char_p),
    ("GetFirmwareVersion", [c_int], c_int),
    ("GetImageSize", [c_int_p, c_int_p, c_int_p, c_int], c_bool),
    ("GetSizeOfPixel", [c_int], c_int),
    ("DllIsBusy", [c_int], c_bool),
    ("GetMaxExposureTime", [c_int], c_int),
    ("GetMaxBinningX", [c_int_p, c_int], c_int),
    ("GetMaxBinningY", [c_int_p, c_int], c_int),
    ("SupportedSensorFeature", [c_int, c_int_p, c_int], c_bool),
    ("GetNumberOfSensorOutputModes", [c_int], c_int),
    ("GetSensorOutputModeStrings", [c_int, c_int], c_char_p),
    ("GetLastMeasTimeNeeded", [c_int], c_float),
    ("TemperatureControl_Init", [c_int, c_int_p, c_int_p, c_int_p, c_int], c_int),
    ("TemperatureControl_GetTemperature", [c_int, c_int_p, c_int_p, c_int], c_bool),
    ("TemperatureControl_SetTemperature", [c_int, c_int_p, c_int], c_bool),
    ("TemperatureControl_SwitchOff", [c_int_p, c_int], c_bool),
    ("StartMeasurement_DynBitDepth", [c_bool, c_bool, c_bool, c_bool, c_int_p, c_int], c_bool),
    ("GetMeasurementData_DynBitDepth", [c_void_p, c_int_p, c_int], c_bool),
    ("PerformMeasurement_Blocking_DynBitDepth", [c_bool, c_bool, c_bool, c_bool,
                                                 c_int, c_void_p, c_int_p, c_int], c_bool),
    ("StopMeasurement", [c_int], c_bool)
]


class GELibrary:
    def __init__(self):
        if os.name == "nt":
            libpath = os.path.join(os.path.dirname(__file__), "greateyes.dll")
            self.dll = windll.LoadLibrary(libpath)
        else:
            libpath = os.path.join(os.path.dirname(__file__), "libgreateyes.so")
            self.dll = CDLL(libpath)
        # wrap the api functions
        for api_func_name, argtypes, restype in _API_FUNCTION_PROTOTYPES:
            api_func = self.dll.__getattr__(api_func_name)
            api_func.argtypes = argtypes
            api_func.restype = restype
        size = c_int(0)
        self.dll_version = self.dll.GetDLLVersion(byref(size))
        if size != 0:
            print(f"DLL {self.dll_version} loaded correctly")
