#! /usr/bin/env python

import tkinter as Tk
from tkinter import messagebox
from threading import Thread

from .ge_camera import GECamera
from .ge_camera import FakeCamera
from .config import ConfigParams
from .temp_corr import TCorrWrapper
from .GUI.funcPanel import FuncPanel
from .GUI.imagPanel import ImagPanel
from .GUI.devicesPanel import DevicesPanel
from .GUI.grabPanel import GrabPanel
from .GUI.statusPanel import StatusPanel
from .GUI.HistogramPanel import HistogramPanel
from .azt_onboard_client import AZT_Onboard


class MainApplication(Tk.Frame):
    def __init__(self, debug_camera, debug_pi, pro_mode):
        self.config_params = ConfigParams()
        # Program-wide definitions
        self.path_to_save_files = self.config_params.params["path_to_save_files"]
        self.dark_number = 0
        self.alarm_active = True
        self.observer_name = ""
        self.condition_notes = ""
        self.alive = True
        self.dev_list = []
        # popup_exists variable is True if some popup window exists already
        # to prevent several popups to be invoked simultaneously
        self.popup_exists = False

        # Create root window
        # Create main window base frame
        self.root = Tk.Tk()

        # Connect to AZT onboard server
        if debug_pi is True:
            self.azt = AZT_Onboard(self, "127.0.0.1", 6546)
        else:
            self.azt = AZT_Onboard(self, "192.168.1.16", 6546)
        Thread(target=self.azt.connect).start()
        self.root.after(0, self.wait_for_azt_connection)

        # Connect to GreatEye camera
        if debug_camera is True:
            self.gecamera = FakeCamera(cam_addr=0, ether_addr="192.168.1.234")
        else:
            self.gecamera = GECamera(cam_addr=0, ether_addr="192.168.1.234")

        # Configure root window
        self.root.title("GEOps")
        self.root.protocol('WM_DELETE_WINDOW', self.shutdown)
        self.root.resizable(0, 0)

        # Load a function buttons panel
        self.funcPanel = FuncPanel(self)
        # Load an images panel
        self.imagPanel = ImagPanel(self)
        # Load a panel for controlling focus and filter wheels
        self.devicesPanel = DevicesPanel(self, pro_mode)
        # Load a panel for image grabbing
        self.grabPanel = GrabPanel(self)
        # Load a histogram panel
        self.histogramPanel = HistogramPanel(self)
        # Load a status panel
        self.statusPanel = StatusPanel(self, pro_mode)

        # Create a temperature correction wrapper instance
        self.tcorr_wrapper = TCorrWrapper(self)

        self.root.after(500, self.devicesPanel.focuser_rotator_controls.wait_focuser)
        self.root.after(500, self.devicesPanel.focuser_rotator_controls.wait_rotator)
        # Start camera initiating
        self.root.after(100, self.devicesPanel.connect_wheels)
        self.root.after(200, self.camera_connection_cycle)
        self.root.after(2000, self.imagPanel.show_image_cycle)
        self.root.mainloop()

    def camera_connection_cycle(self):
        """
        This function waits for the camera to be connected and when it is:
        1) changes status message
        2) starts the job for the camera info (like temperature) update
        3) Unlock camera related interface options
        """
        if (not self.gecamera.connected) and self.gecamera.keep_connected:
            self.statusPanel.camera_status_value.set("Connecting")
            self.statusPanel.camera_status_label.config(fg="black")
            if not self.gecamera.connecting:
                Thread(target=self.gecamera.connect_via_ethernet).start()

        if self.gecamera.connected:
            # Connection was established
            # 1) Show this on the status panel
            self.statusPanel.camera_status_value.set("Connected")
            self.statusPanel.camera_status_label.config(fg="black")
            # 2) Start the job for the camera info
            # start a background job for showing the camera temperatures
            if self.statusPanel.update_info_is_working is False:
                Thread(target=self.statusPanel.update_info).start()
            # 3) Unlock camera-related buttons
            if self.grabPanel.grabbing_state == "Disconnected":
                self.funcPanel.camera_unlock()
                self.grabPanel.set_idle_mode()

        else:
            self.statusPanel.camera_status_value.set("Disconnected")
            self.statusPanel.camera_status_label.config(fg="red")
            self.funcPanel.camera_lock()
            self.grabPanel.set_disconnected_mode()
        self.root.after(100, self.camera_connection_cycle)
        # try:
        #     self.gecamera.connect_via_ethernet("192.168.1.234")
        # except DeviceIsNotAvailableError as e:
        #     # Camera is not responding. Let's shut down everything and exit
        #     self.root.withdraw()
        #     messagebox.showerror(parent=self.root, title="Error", message=str(e))
        #     self.filterwheel_photo.shutdown()
        #     self.filterwheel_polar.shutdown()
        #     self.focuser_rotator.shutdown()
        #     exit(1)

    def wait_for_azt_connection(self):
        """
        This function waits for the azt onboard to be connected and when it is:
        1) changes status message
        """
        if self.azt.is_running and self.azt.active_connection:
            # Connection was established
            # 1) Show this on the status panel
            msg = "Connected"
            new_dev_list = []
            if (
                    'focuser' in self.azt.state and
                    'Connected' in self.azt.state['focuser'] and
                    self.azt.state['focuser']['Connected'] is True
            ):
                new_dev_list.append('F')

            if (
                    'rotator' in self.azt.state and
                    'Connected' in self.azt.state['rotator'] and
                    self.azt.state['rotator']['Connected'] is True
            ):
                new_dev_list.append('R')

            if (
                    'wheel_1' in self.azt.state and
                    'Connected' in self.azt.state['wheel_1'] and
                    self.azt.state['wheel_1']['Connected'] is True and
                    self.azt.state['wheel_1']['Filter_pos'] is not None
            ):
                new_dev_list.append('W1')

            if (
                    'wheel_2' in self.azt.state and
                    'Connected' in self.azt.state['wheel_2'] and
                    self.azt.state['wheel_2']['Connected'] is True and
                    self.azt.state['wheel_2']['Filter_pos'] is not None
            ):
                new_dev_list.append('W2')

            if new_dev_list != self.dev_list:
                # The devices list has changed since the last check
                msg += " (" + ", ".join(new_dev_list) + ")"
                if self.grabPanel.grab_lock_on is False:
                    if (
                            "F" in new_dev_list and
                            not self.devicesPanel.focuser_rotator_controls.focuser_moving and
                            not self.devicesPanel.focuser_rotator_controls.waiting_for_stop_moving
                    ):
                        self.devicesPanel.focuser_rotator_controls.focuser_unlock()
                    else:
                        self.devicesPanel.focuser_rotator_controls.focuser_lock()
                    if "R" in new_dev_list:
                        self.devicesPanel.focuser_rotator_controls.rotator_unlock()
                    else:
                        self.devicesPanel.focuser_rotator_controls.rotator_lock()
                    if "W1" in new_dev_list:
                        if self.devicesPanel.filterwheel_photo_controls.in_action:
                            self.devicesPanel.filterwheel_photo_controls.set_status("Moving")
                        else:
                            self.devicesPanel.filterwheel_photo_controls.set_status("Idle")
                    else:
                        self.devicesPanel.filterwheel_photo_controls.set_status("Disconnected")
                    if "W2" in new_dev_list:
                        if self.devicesPanel.filterwheel_polar_controls.in_action:
                            self.devicesPanel.filterwheel_polar_controls.set_status("Moving")
                        else:
                            self.devicesPanel.filterwheel_polar_controls.set_status("Idle")
                    else:
                        self.devicesPanel.filterwheel_polar_controls.set_status("Disconnected")
                self.dev_list = new_dev_list[:]
                self.statusPanel.onboard_status_value.set(msg)
                self.statusPanel.onboard_status_label.config(fg="black")
            self.root.after(1000, self.wait_for_azt_connection)
        else:
            self.dev_list = []
            # Connection was not established yet, or was lost.
            # Show this situation in the status message
            self.statusPanel.onboard_status_value.set("Disconnected")
            self.statusPanel.onboard_status_label.config(fg="red")
            # Lock devices, as they are not available when the focuser is locked
            self.devicesPanel.focuser_rotator_controls.lock()
            self.devicesPanel.filterwheel_photo_controls.set_status("N/A", force=True)
            self.devicesPanel.filterwheel_polar_controls.set_status("N/A", force=True)
            self.root.after(1000, self.wait_for_azt_connection)

    def shutdown(self):
        """
        This function is called when the user hit close window button
        """
        answer = messagebox.askokcancel("Close", "Really close?")
        if answer is True:
            self.alive = False
            # Shutting down devices
            self.gecamera.close_connection()
            self.azt.send_command("server", "disconnect_all_devices", ())
            self.azt.close()
            self.devicesPanel.filterwheel_photo_controls.status_message.set("Closing")
            # self.devicesPanel.filterwheel_polar_controls.status_message.set("Closing")
            # Stopping background processes
            self.imagPanel.image_queue.put(None)
            # Close the main window
            self.root.after(1000, self.close)

    def close(self):
        self.root.destroy()
        # else:
        #     self.root.after(100, self.close)
