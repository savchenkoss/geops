#! /usr/bin/env python

import tkinter as Tk
from threading import Thread
import queue
import pylab
import numpy as np
from matplotlib.colors import LogNorm
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib import rcParams

rcParams["ps.useafm"] = False
rcParams["pdf.use14corefonts"] = False
rcParams["text.usetex"] = False


class ImagPanel(Tk.Frame):
    def __init__(self, window):
        self.window = window
        self.panel = Tk.Frame(self.window.root)
        self.timeout = 0.5
        self.panel.grid(column=0, row=1, rowspan=6)
        self.mainGraph = pylab.Figure(figsize=(5.344, 5.50), dpi=96)
        self.canvas = FigureCanvasTkAgg(self.mainGraph, master=self.panel)
        self.canvas.mpl_connect('button_press_event', self.onclick)
        self.fig = self.mainGraph.add_subplot(111)
        self.fig.tick_params(axis='both', which='major', length=7, labelbottom=False, labelleft=False)
        self.fig.set_title("")
        self.tk_widget = self.canvas.get_tk_widget()
        self.tk_widget.grid(column=0, row=0)
        self.set_new_size(528)
        self.fitsPlotInstance = None
        self.image_queue = queue.Queue()
        self.intensity_value = None
        self.data = None
        self.lower_limit = None
        self.upper_limit = None

    def set_new_size(self, new_size):
        """
        A set of operations to make when the image size were changed
        """
        self.current_figure_x_size = new_size
        self.tk_widget.config(width=min(800, new_size))
        self.tk_widget.config(height=min(810, new_size+10))
        self.fig.axes.set_xticks([new_size/2])
        self.fig.axes.set_yticks([new_size/2])
        # self.mainGraph.subplots_adjust(left=0.02, bottom=0.03, right=0.99, top=0.95)
        self.mainGraph.tight_layout(pad=0.25)
        self.canvas.draw()

    def show_image_cycle(self):
        """
        This function is running in the background waiting for a new
        image to appear in a queue. When the new image is put to the queue
        by some other process, the function plots it.
        """
        if self.image_queue.empty():
            self.window.root.after(100, self.show_image_cycle)
            return
        try:
            # Look if there is a new image in a plot queue
            new_image_and_name = self.image_queue.get(timeout=self.timeout)
        except:
            # Plot queue is empty. Set this function for calling in a 100 ms
            # and retun to the main window flow
            self.window.root.after(100, self.show_image_cycle)
            return
        self.timeout = 0.01
        if new_image_and_name is None:
            # This means we want to end the process
            return
        # We have got a new image to plot, so let's do it right away
        # Remove the old plot at first
        if self.fitsPlotInstance is not None:
            self.fitsPlotInstance.remove()
            self.fitsPlotInstance = None
        if not isinstance(new_image_and_name[0], str):
            data, name = new_image_and_name  # Unpack data to plot
            data[data < 1] = 1  # Get rid of zero and negative values
            ySize, xSize = data.shape
            if self.current_figure_x_size != xSize:
                # image size has been changed
                self.set_new_size(xSize)
            self.meanValue = np.mean(data)
            stdValue = np.std(data)
            self.lower_limit = self.meanValue - 1 * stdValue
            self.upper_limit = self.meanValue + 5 * stdValue
            self.fitsPlotInstance = self.fig.imshow(data, interpolation='nearest', cmap='gray',
                                                    norm=LogNorm(vmin=self.lower_limit, vmax=self.upper_limit),
                                                    origin='upper')
            self.fig.set_title(name)
            self.fig.axis([xSize, 0, 0, ySize])
            self.data = data
        else:
            ySize, xSize = self.data.shape
            self.fitsPlotInstance = self.fig.imshow(self.data, interpolation='nearest', cmap='gray',
                                                    norm=LogNorm(vmin=self.lower_limit, vmax=self.upper_limit),
                                                    origin='upper')
            self.fig.axis([xSize, 0, 0, ySize])
        self.mainGraph.tight_layout(pad=0.25)
        self.canvas.draw()
        self.window.root.after(100, self.show_image_cycle)
        self.window.histogramPanel.update_plot()

    def onclick(self, event):
        if (event.xdata is not None) and (event.ydata is not None) and (self.data is not None):
            intens = self.data[int(event.ydata)][int(event.xdata)]
            self.window.statusPanel.intensity_value.set("%i" % intens)
