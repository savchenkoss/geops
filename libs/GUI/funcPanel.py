#! /usr/bin/env python

import time
from pathlib import Path
import tkinter as Tk
from tkinter import messagebox
from os import path
from threading import Thread
from tkinter import filedialog
from queue import Queue
from queue import Empty
from functools import partial
from astropy.io import fits

from .. import honk


class FuncPanel(Tk.Frame):
    def __init__(self, window):
        self.window = window
        self.panel = Tk.Frame(self.window.root)
        self.panel.grid(column=0, row=0, sticky=Tk.N)

        self.camera_locked = True  # True if locked camera-related elements
        self.fully_locked = False  # Trye if locked all elements

        # Create buttons
        # Setup button
        self.setup_button = Tk.Button(self.panel, text="Setup", font=("bold"),
                                      command=self.show_setup_window)
        self.setup_button.grid(column=0, row=0)

        # Take bias button
        self.bias_button = Tk.Button(self.panel, text="Bias", font=("bold"),
                                     command=self.show_bias_window, state="disabled")
        self.bias_button.grid(column=1, row=0)

        # Take dark button
        self.dark_button = Tk.Button(self.panel, text="Dark", font=("bold"),
                                     command=self.show_grab_dark_window, state="disabled")
        self.dark_button.grid(column=2, row=0)

        # Focus button
        self.focus_button = Tk.Button(self.panel, text="Focus", font=("bold"),
                                      command=self.show_focus_window, state="disabled")
        self.focus_button.grid(column=3, row=0)

        # Clean button
        # self.clean_button = Tk.Button(self.panel, text="Clean", font=("bold"),
        #                               command=self.clean)
        # self.clean_button.grid(column=4, row=0)

        # Next button
        self.next_button = Tk.Button(self.panel, text="Next", font=("bold"),
                                     command=self.set_next, state='disabled')
        self.next_button.grid(column=4, row=0)

        # 'Save as' button to save last taken frame
        self.last_data_and_header = None
        self.save_as_button = Tk.Button(self.panel, text="Save as", font=("bold"),
                                        command=self.save_as)
        self.save_as_button.grid(column=5, row=0)

        # Devices button
        self.devices_button = Tk.Button(self.panel, text="Devices", font=("bold"), command=self.show_devices_dialog)
        self.devices_button.grid(column=6, row=0)

    def lock(self):
        if self.fully_locked is True:
            # Already locked
            return
        self.setup_button.config(state='disabled')
        self.bias_button.config(state='disabled')
        self.dark_button.config(state='disabled')
        self.focus_button.config(state='disabled')
        self.next_button.config(state='normal')
        self.save_as_button.config(state='disabled')
        self.devices_button.config(state='disabled')
        self.fully_locked = True
        self.camera_locked = True

    def unlock(self):
        if (self.fully_locked is False) and (self.camera_locked is False):
            return
        self.setup_button.config(state='normal')
        self.bias_button.config(state='normal')
        self.dark_button.config(state='normal')
        self.focus_button.config(state='normal')
        self.next_button.config(state='disabled')
        self.save_as_button.config(state='normal')
        self.devices_button.config(state='normal')
        self.camera_locked = False
        self.fully_locked = False

    def camera_lock(self):
        """
        Lock only buttons that are related to the camera
        """
        if self.camera_locked is True:
            # Already locked
            return
        self.bias_button.config(state='disabled')
        self.dark_button.config(state='disabled')
        self.focus_button.config(state='disabled')
        self.camera_locked = True

    def camera_unlock(self):
        """
        Lock only buttons that are related to the camera
        """
        if self.camera_locked is False:
            # Already unlocked
            return
        self.bias_button.config(state='normal')
        self.dark_button.config(state='normal')
        self.focus_button.config(state='normal')
        self.camera_locked = False

    def show_setup_window(self):
        if not self.window.popup_exists:
            SetupWindow(self.window)

    def show_bias_window(self):
        if not self.window.popup_exists:
            GrabBiasWindow(self.window)

    def show_grab_dark_window(self):
        if not self.window.popup_exists:
            GrabDarkWindow(self.window)

    def show_focus_window(self):
        if not self.window.popup_exists:
            FocusWindow(self.window)

    def set_next(self):
        if not self.window.popup_exists:
            SetNextWindow(self.window)

    def save_as(self):
        if self.window.popup_exists:
            return
        self.window.popup_exists = True
        if self.last_data_and_header is None:
            messagebox.showwarning(parent=self.window.root, title="Warning",
                                   message="Take an image first!")
            self.window.popup_exists = False
            return False
        file_name = filedialog.asksaveasfilename(parent=self.window.root,
                                                 defaultextension=".fit",
                                                 confirmoverwrite=True,
                                                 initialdir=self.window.path_to_save_files,
                                                 filetypes=[["FITS", "*.fit"], ["All", "*"]])
        if file_name:
            hdu = fits.PrimaryHDU(data=self.last_data_and_header[0], header=self.last_data_and_header[1])
            hdu.header["BSCALE"] = 1
            hdu.header["BZERO"] = 0
            hdu.writeto(file_name, overwrite=True)
        self.window.popup_exists = False

    def show_devices_dialog(self):
        if not self.window.popup_exists:
            DevicesDialogWindow(self.window)


class SetupWindow(Tk.Frame):
    """
    A window with some preferences that appears when "Setup" button is pressed
    """
    def __init__(self, window):
        self.window = window
        self.top = Tk.Toplevel(self.window.root)
        self.top.title("Setup")
        self.top.geometry('+%i+%i' % (self.window.root.winfo_x() + 80,
                                      self.window.root.winfo_y() + 80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.window.popup_exists = True
        self.top.protocol('WM_DELETE_WINDOW', self.onclose)

        # Directory
        self.working_directory_value = Tk.StringVar()
        self.working_directory_value.set(self.window.path_to_save_files)
        Tk.Label(self.top, text="Directory:").grid(column=0, row=0, sticky=Tk.W, padx=5, pady=5)
        Tk.Entry(self.top, textvariable=self.working_directory_value).grid(column=0, row=1, padx=5)
        Tk.Button(self.top, text="Choose", command=self.select_directory).grid(column=1, row=1, padx=5, sticky=Tk.W)

        # Cooler temperature
        Tk.Label(self.top, text="CCD temperature").grid(column=0, row=2, padx=10, pady=5, sticky=Tk.E)
        self.ccd_temperature_value = Tk.StringVar()
        if self.window.gecamera.current_set_temperature is not None:
            self.ccd_temperature_value.set(round(self.window.gecamera.current_set_temperature))
        Tk.Entry(self.top, textvariable=self.ccd_temperature_value, width=5).grid(column=1, row=2, padx=10,
                                                                                  sticky=Tk.W, pady=5)
        # Readout frequency
        Tk.Label(self.top, text="Readout freq.").grid(column=0, row=3, padx=10, pady=5, sticky=Tk.E)
        freq_options = ["50_kHz", "100_kHz", "250_kHz", "500_kHz", "1_MHz", "3_MHz"]
        self.frequency_value = Tk.StringVar()
        current_readout_speed = self.window.gecamera.current_mode_string
        self.frequency_value.set(current_readout_speed)
        self.freq_menu = Tk.OptionMenu(self.top, self.frequency_value, *freq_options)
        self.freq_menu.config(width=6)
        self.freq_menu.grid(column=1, row=3, pady=5, padx=5, sticky=Tk.W)

        # Bin size radiobutton
        Tk.Label(self.top, text="Bin size").grid(column=0, row=4, rowspan=2, padx=10, pady=5, sticky=Tk.E)
        self.bin_size_value = Tk.IntVar()
        Tk.Radiobutton(self.top, text="1", variable=self.bin_size_value, value=1).grid(column=1, row=4, sticky=Tk.W)
        Tk.Radiobutton(self.top, text="2", variable=self.bin_size_value, value=2).grid(column=1, row=5, sticky=Tk.W)
        self.bin_size_value.set(self.window.gecamera.current_binning)

        # Alarm checkbox
        Tk.Label(self.top, text="Alarm").grid(column=0, row=6, padx=10, pady=5, sticky=Tk.E)
        self.alarm_value = Tk.IntVar()
        if self.window.alarm_active is True:
            self.alarm_value.set(1)
        else:
            self.alarm_value.set(0)
        Tk.Checkbutton(self.top, variable=self.alarm_value).grid(column=1, row=6, padx=5,
                                                                 pady=5, sticky=Tk.W)

        # Observer name entry
        Tk.Label(self.top, text="Observer: ").grid(column=0, row=7, padx=10, pady=5, sticky=Tk.E)
        self.observer_name_value = Tk.StringVar()
        if self.window.observer_name:
            self.observer_name_value.set(self.window.observer_name)
        entry = Tk.Entry(self.top, textvariable=self.observer_name_value, width=16)
        entry.grid(column=1, row=7, padx=5, pady=5, sticky=Tk.W)

        # Notes entry
        Tk.Label(self.top, text="Notes: ").grid(column=0, row=8, padx=10, pady=5, sticky=Tk.E)
        self.condition_notes_value = Tk.StringVar()
        if self.window.condition_notes:
            self.condition_notes_value.set(self.window.condition_notes)
        entry = Tk.Entry(self.top, textvariable=self.condition_notes_value, width=16)
        entry.grid(column=1, row=8, padx=5, pady=5, sticky=Tk.W)

        # Rotator zero point
        Tk.Label(self.top, text="Rotator offset").grid(column=0, row=9, padx=10, pady=5, sticky=Tk.E)
        self.rotator_zero_point_value = Tk.StringVar()
        self.rotator_zero_point_value.set(self.window.config_params.params["rotator_zero_point"])
        entry = Tk.Entry(self.top, textvariable=self.rotator_zero_point_value, width=5, state="normal")
        entry.grid(column=1, row=9, padx=5, pady=5, sticky=Tk.W)

        # Apply button
        Tk.Button(self.top, text="Apply", command=self.apply_changes).grid(column=0, row=10, pady=10)

        # Cancel button
        Tk.Button(self.top, text="Cancel", command=self.onclose).grid(column=1, row=10, pady=10)

        # Override destroy method
        self.top.protocol("WM_DELETE_WINDOW", self.onclose)

    def select_directory(self):
        new_directory = filedialog.askdirectory(parent=self.top,
                                                initialdir=self.working_directory_value.get())
        if new_directory:
            self.working_directory_value.set(new_directory)

    def apply_changes(self):
        # Validate values before applying changes
        validate_result = self.validate()
        if validate_result is False:
            return
        # Set new path to working directory
        self.window.path_to_save_files = self.working_directory_value.get()
        parent = str(Path(self.window.path_to_save_files).absolute().parent)
        self.window.config_params.update_parameter("path_to_save_files", parent)
        # Set cooler temperature
        self.window.gecamera.temperature_control_set_temperature(round(float(self.ccd_temperature_value.get())))
        # Set frequency mode
        # freq_options = ["50_kHz", "100_kHz", "250_kHz", "500_kHz", "1_MHz", "3_MHz", "5_MHz"]
        self.window.gecamera.set_frequency_mode(self.frequency_value.get())
        # Set binning
        self.window.gecamera.set_binning(self.bin_size_value.get())
        # Set observers name
        self.window.observer_name = self.observer_name_value.get()
        # Set condition notes
        self.window.condition_notes = self.condition_notes_value.get()
        # Set alarm
        if self.alarm_value.get() == 1:
            self.window.alarm_active = True
        else:
            self.window.alarm_active = False
        # Set rotator zero point
        self.window.config_params.update_parameter("rotator_zero_point", float(self.rotator_zero_point_value.get()))
        self.onclose()

    def validate(self):
        """
        Validate entry values before applying changes
        """
        # Validate CCD temperature settings
        try:
            float(self.ccd_temperature_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning",
                                   message="Wrong CCD temperature value!")
            return False
        if float(self.ccd_temperature_value.get()) < -80:
            messagebox.showwarning(parent=self.top, title="Warning",
                                   message="CCD temperature target should be higher than -80 degrees!")
            return False
        # Validate rotator zero point value
        try:
            float(self.rotator_zero_point_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning",
                                   message="Wrong rotator zero point value!")
            return False
        if not (-180 <= float(self.rotator_zero_point_value.get()) <= 180):
            messagebox.showwarning(parent=self.top, title="Warning",
                                   message="Rotator zero point value should be between -180 and 180!")
            return False
        return True

    def onclose(self):
        self.window.popup_exists = False
        self.top.destroy()

    def reset_tcorr(self):
        answer = messagebox.askokcancel("Reset", "Clear all temperature corrections data?",
                                        parent=self.top)
        if answer is True:
            self.window.tcorr_wrapper.reset()


class GrabBiasWindow(Tk.Frame):
    """
    Grab bias frames window
    """
    def __init__(self, window):
        self.window = window
        self.top = Tk.Toplevel(self.window.root)
        self.top.title("   Grab bias")
        self.top.geometry('+%i+%i' % (self.window.root.winfo_x()+80,
                                      self.window.root.winfo_y()+80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.window.popup_exists = True

        # Bias name entry
        Tk.Label(self.top, text="   Bias name:").grid(column=0, row=0, padx=10)
        self.bias_name_value = Tk.StringVar()
        self.bias_name_value.set("bias")
        Tk.Entry(self.top, textvariable=self.bias_name_value, width=7).grid(column=1, row=0, padx=15)

        # Bias count entry
        Tk.Label(self.top, text="Count:").grid(column=0, row=1, padx=10)
        self.bias_count_value = Tk.StringVar()
        self.bias_count_value.set(str(5))
        Tk.Entry(self.top, textvariable=self.bias_count_value, width=7).grid(column=1, row=1)

        # Start button
        Tk.Button(self.top, text="Start", command=self.start).grid(column=0, row=2, pady=10)

        # Cancel button
        Tk.Button(self.top, text="Cancel", command=self.onclose).grid(column=1, row=2, pady=10)

        # Override destroy method
        self.top.protocol("WM_DELETE_WINDOW", self.onclose)

    def validate(self):
        try:
            count = int(self.bias_count_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning", message="Wrong count value!")
            return False
        if count <= 0:
            messagebox.showwarning(parent=self.top, title="Warning", message="Count must be positive!")
            return False
        return True

    def start(self):
        validate_result = self.validate()
        if validate_result is False:
            return
        Thread(target=self.grab_biases).start()

    def grab_biases(self):
        # Get values from entries
        bias_common_name = self.bias_name_value.get()
        num_of_biases = int(self.bias_count_value.get())

        # Let's check at first, if we are goig to overwrite existing files
        for idx in range(0, num_of_biases):
            out_name = path.join(self.window.path_to_save_files, "%sS%i.FIT" % (bias_common_name, idx))
            if path.exists(out_name):
                answer = messagebox.askokcancel("Files exist already", "Overwrite existing files?")
                if answer is False:
                    # The user has decided not to ovewrite files. Return to the grab control window
                    return
                else:
                    # The user has decided to overwite existing files. Proceed with the function
                    break

        # We are going to grab darks now, so the control window is not needed anymore
        self.onclose()

        # Lock func and grab panels
        Thread(target=self.window.funcPanel.lock).start()
        Thread(target=self.window.grabPanel.set_grabbing_mode).start()

        # Grab darks
        self.window.grabPanel.desired_exposures_value.set(num_of_biases)
        for idx in range(0, num_of_biases):
            self.window.grabPanel.current_expusure_number_value.set(idx+1)
            # Bias has zero exposure, but let's set some shurt time duration for
            # progress bar, so it blinks real quick and user is happy with the animation
            self.window.grabPanel.run_progress_bar_in_background([0.1, self.window.gecamera.current_readout_timing],
                                                                 ["blue", "gray"])
            # Shot dark frame and retrieve it from camera
            bias_image = self.window.gecamera.grab_dark(1.0)
            if bias_image is None:
                # Exposure was stopped
                break
            # Fill header
            header = fits.header.Header()
            self.window.gecamera.fill_header(header)
            header["IMAGETYP"] = 'Bias Frame'
            header["OBSERVER"] = (self.window.observer_name.strip(), "Name of the observer")
            header["NOTES"] = (self.window.condition_notes.strip(), "Observations conditions")
            temp = self.window.devicesPanel.focuser_rotator_controls.focuser_temperature
            header["TOUTSIDE"] = (temp, "Outside temperature, C")
            # Save file
            out_name = path.join(self.window.path_to_save_files, "%sS%i.FIT" % (bias_common_name, idx))
            hdu = fits.PrimaryHDU(data=bias_image, header=header)
            hdu.header["BSCALE"] = 1
            hdu.header["BZERO"] = 0
            hdu.writeto(out_name, overwrite=True)
            # Show image
            self.window.imagPanel.image_queue.put((bias_image, path.basename(out_name)))
            # Store the image so it can be manually saved
            self.window.funcPanel.last_data_and_header = (bias_image, header)
            time.sleep(1.0)
        # Remove the progress information from the grab panel
        self.window.grabPanel.current_expusure_number_value.set("-")
        self.window.grabPanel.desired_exposures_value.set("-")
        # Unlock func and grab panels
        self.window.funcPanel.unlock()
        self.window.grabPanel.set_idle_mode()

    def onclose(self):
        self.window.popup_exists = False
        self.top.destroy()


class GrabDarkWindow(Tk.Frame):
    """
    Grab dark frames window
    """
    def __init__(self, window):
        self.window = window
        self.top = Tk.Toplevel(self.window.root)
        self.top.title("   Grab dark")
        self.top.geometry('+%i+%i' % (self.window.root.winfo_x()+80,
                                      self.window.root.winfo_y()+80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.window.popup_exists = True

        # Dark name entry
        Tk.Label(self.top, text="   Dark name:").grid(column=0, row=0, padx=10)
        self.dark_name_value = Tk.StringVar()
        self.dark_name_value.set("dark%i" % (self.window.dark_number+1))
        Tk.Entry(self.top, textvariable=self.dark_name_value, width=7).grid(column=1, row=0, padx=15)

        # Dark exposure entry
        Tk.Label(self.top, text="  Exposure:").grid(column=0, row=1, padx=10)
        self.dark_exposure_length_value = Tk.StringVar()
        self.dark_exposure_length_value.set(60)
        Tk.Entry(self.top, textvariable=self.dark_exposure_length_value, width=7).grid(column=1, row=1)

        # Dark count entry
        Tk.Label(self.top, text="Count:").grid(column=0, row=2, padx=10)
        self.dark_count_value = Tk.StringVar()
        self.dark_count_value.set(str(5))
        Tk.Entry(self.top, textvariable=self.dark_count_value, width=7).grid(column=1, row=2)

        # Start button
        Tk.Button(self.top, text="Start", command=self.start).grid(column=0, row=3, pady=10)

        # Cancel button
        Tk.Button(self.top, text="Cancel", command=self.onclose).grid(column=1, row=3, pady=10)

        # Override destroy method
        self.top.protocol("WM_DELETE_WINDOW", self.onclose)

    def validate(self):
        try:
            exposure = float(self.dark_exposure_length_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning", message="Wrong exposure length value!")
            return False
        if exposure < 0:
            messagebox.showwarning(parent=self.top, title="Warning", message="Exposure length must be positive!")
            return False
        try:
            count = int(self.dark_count_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning", message="Wrong count value!")
            return False
        if count <= 0:
            messagebox.showwarning(parent=self.top, title="Warning", message="Count value must be positive!")
            return False
        return True

    def start(self):
        validate_result = self.validate()
        if validate_result is False:
            return
        Thread(target=self.grab_darks).start()

    def grab_darks(self):
        # Get the values from the entries
        num_of_darks = int(self.dark_count_value.get())
        dark_exposure_length = float(self.dark_exposure_length_value.get())
        dark_common_name = self.dark_name_value.get()

        # Let's check at first, if we are goig to overwrite existing files
        for idx in range(0, num_of_darks):
            out_name = path.join(self.window.path_to_save_files, "%sS%i.FIT" % (dark_common_name, idx))
            if path.exists(out_name):
                answer = messagebox.askokcancel("Files exist already", "Overwrite existing files?")
                if answer is False:
                    # The user has decided not to ovewrite files. Return to the grab control window
                    return
                else:
                    # The user has decided to overwite existing files. Proceed with the function
                    break

        # We are going to grab darks now, so the control window is not needed anymore
        self.onclose()

        # Lock func and grab panels
        Thread(target=self.window.funcPanel.lock).start()
        Thread(target=self.window.grabPanel.set_grabbing_mode).start()

        # Grab darks
        self.window.grabPanel.desired_exposures_value.set(num_of_darks)
        for idx in range(0, num_of_darks):
            self.window.grabPanel.current_expusure_number_value.set(idx + 1)
            delay = self.window.gecamera.current_readout_timing
            self.window.grabPanel.run_progress_bar_in_background([dark_exposure_length, delay], ["blue", "gray"])
            # Shot dark frame and retrieve it from camera
            dark_image = self.window.gecamera.grab_dark(1000 * dark_exposure_length)

            if dark_image is None:
                # Exposure was stopped
                break
            # Fill header
            header = fits.header.Header()
            self.window.gecamera.fill_header(header)
            header["IMAGETYP"] = 'Dark Frame'
            header["OBSERVER"] = (self.window.observer_name.strip(), "Name of the observer")
            header["NOTES"] = (self.window.condition_notes.strip(), "Observations conditions")
            temp = self.window.devicesPanel.focuser_rotator_controls.focuser_temperature
            header["TOUTSIDE"] = (temp, "Outside temperature, C")
            # Save file
            out_name = path.join(self.window.path_to_save_files, "%sS%i.FIT" % (dark_common_name, idx))
            hdu = fits.PrimaryHDU(data=dark_image, header=header)
            hdu.header["BSCALE"] = 1
            hdu.header["BZERO"] = 0
            hdu.writeto(out_name, overwrite=True)
            # Show image
            self.window.imagPanel.image_queue.put((dark_image, path.basename(out_name)))
            # Store the image so it can be manually saved
            self.window.funcPanel.last_data_and_header = (dark_image, header)
            if self.window.alarm_active and (idx == (num_of_darks - 2)):
                # Second to last exposure just finished
                honk.honk(500, 500)
            elif self.window.alarm_active and (idx == (num_of_darks - 1)):
                # Last exposure just finished
                honk.honk(750, 500)

        else:
            # Extract dark name number from the name given to use it next time to
            # create a new name. This will be executed only if the exposition was not aborted
            self.window.dark_number = int("".join([c for c in dark_common_name if c.isnumeric()]))
        # Set status to idling
        # Remove the progress information from the grab panel
        self.window.grabPanel.current_expusure_number_value.set("-")
        self.window.grabPanel.desired_exposures_value.set("-")
        # Unlock func and grab panels
        self.window.funcPanel.unlock()
        self.window.grabPanel.set_idle_mode()

    def onclose(self):
        self.window.popup_exists = False
        self.top.destroy()


class FocusWindow(Tk.Frame):
    """
    The window for configuring focus parameters
    """
    def __init__(self, window):
        self.window = window
        self.top = Tk.Toplevel(self.window.root)
        self.top.title("Focus")
        self.top.geometry('+%i+%i' % (self.window.root.winfo_x()+80,
                                      self.window.root.winfo_y()+80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.window.popup_exists = True

        # Exposure entry
        Tk.Label(self.top, text="Exposure: ").grid(column=0, row=0, padx=10, pady=10)
        self.exposure_value = Tk.StringVar()
        self.exposure_value.set(2)
        self.exposure_entry = Tk.Entry(self.top, textvariable=self.exposure_value, width=6)
        self.exposure_entry.grid(column=1, row=0, padx=15, columnspan=2)
        self.exposure_entry.bind("<Return>", lambda e: self.start())
        self.exposure_entry.focus()

        # Bin size radiobuttons
        Tk.Label(self.top, text="Bin size").grid(column=0, row=1, padx=10, pady=10, sticky=Tk.E)
        self.bin_size_value = Tk.IntVar()
        self.bin_size_value.set(self.window.gecamera.current_binning)
        Tk.Radiobutton(self.top, text="1", variable=self.bin_size_value, value=1).grid(column=1, row=1, sticky=Tk.W)
        Tk.Radiobutton(self.top, text="2", variable=self.bin_size_value, value=2).grid(column=2, row=1, sticky=Tk.W)

        # Start button
        Tk.Button(self.top, text="Start", command=self.start).grid(column=0, row=2, pady=10)

        # Cancel button
        Tk.Button(self.top, text="Cancel", command=self.onclose).grid(column=1, row=2, pady=10, columnspan=2)

        # Override destroy method
        self.top.protocol("WM_DELETE_WINDOW", self.onclose)

    def validate(self):
        try:
            exposure = float(self.exposure_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning", message="Wrong exposure value!")
            return False
        if exposure <= 0:
            messagebox.showwarning(parent=self.top, title="Warning", message="Exposure length must be positive!")
            return False
        return True

    def start(self):
        validate_result = self.validate()
        if validate_result is False:
            return
        self.onclose()
        bin_size = int(self.bin_size_value.get())
        exposure_length = float(self.exposure_value.get())
        image_queue = Queue()
        # Lock func and grab panels
        Thread(target=self.window.funcPanel.lock).start()
        Thread(target=self.window.grabPanel.set_grabbing_mode).start()
        # Start focusing mode on the camera side
        Thread(target=self.window.gecamera.focus_cycle,
               args=(1000*exposure_length, bin_size, image_queue)).start()
        # Start focus processing
        Thread(target=self.process_focusing, args=(exposure_length, image_queue, bin_size)).start()

    def process_focusing(self, exposure_length, image_queue, bin_size):
        timeout = max(5, 2 * exposure_length)
        if bin_size == 1:
            delay = self.window.gecamera.readout_timings_bin1["1_MHz"]
        else:
            delay = self.window.gecamera.readout_timings_bin2["1_MHz"]
        idx = 0
        self.focus_progress = True
        while self.focus_progress is True:
            self.window.grabPanel.run_progress_bar_in_background([exposure_length, delay], ["blue", "gray"])
            idx += 1
            # Wait for a new image, but not longer that for
            try:
                focus_image = image_queue.get(timeout=timeout)
            except Empty:
                # The camera didn't provide a new image for far too long
                break
            if focus_image is None:
                # Focusing aborted
                break
            # Run progress bar for the next image

            # Show image
            self.window.imagPanel.image_queue.put((focus_image, "focus %i" % idx))
            # Store the image so it can be manually saved
            header = fits.header.Header()
            self.window.gecamera.fill_header(header)
            header["IMAGETYP"] = 'Focus Frame'
            header["OBSERVER"] = (self.window.observer_name, "Name of the observer")
            temp = self.window.devicesPanel.focuser_rotator_controls.focuser_temperature
            header["TOUTSIDE"] = (temp, "Outside temperature, C")
            self.window.funcPanel.last_data_and_header = (focus_image, header)
        self.focus_progress = False
        time.sleep(2)
        # Unlock func and grab panels
        self.window.funcPanel.unlock()
        self.window.grabPanel.set_idle_mode()

    def onclose(self):
        self.window.popup_exists = False
        self.top.destroy()


class DevicesDialogWindow(Tk.Frame):
    def __init__(self, window):
        self.window = window
        self.is_alive = True
        self.top = Tk.Toplevel(self.window.root)
        self.top.title("Manage remote devices")
        self.top.geometry('+%i+%i' % (self.window.root.winfo_x()+80,
                                      self.window.root.winfo_y()+80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.window.popup_exists = True

        # Override destroy method
        self.top.protocol("WM_DELETE_WINDOW", self.onclose)

        # Manage camera
        row = 0
        Tk.Label(self.top, text="Camera"). grid(column=0, row=row, sticky=Tk.E, padx=5, pady=5)
        self.connect_camera_button = Tk.Button(self.top, text="Connect", command=self.connect_camera)
        self.connect_camera_button.grid(column=1, row=row, padx=5, pady=5)
        self.disconnect_camera_button = Tk.Button(self.top, text="Disconnect", command=self.disconnect_camera)
        self.disconnect_camera_button.grid(column=2, row=row, padx=5)
        self.camera_status_value = Tk.StringVar()
        self.camera_status_label = Tk.Label(self.top, textvariable=self.camera_status_value, width=12)
        self.camera_status_label.grid(column=3, row=row, sticky=Tk.W)

        # Manage server
        row += 1
        Tk.Label(self.top, text="Server"). grid(column=0, row=row, sticky=Tk.E, padx=5, pady=5)
        self.connect_server_button = Tk.Button(self.top, text="Connect", command=self.connect_server)
        self.connect_server_button.grid(column=1, row=row, padx=5, pady=5)
        self.disconnect_server_button = Tk.Button(self.top, text="Disconnect", command=self.disconnect_server)
        self.disconnect_server_button.grid(column=2, row=row, padx=5)
        self.server_status_value = Tk.StringVar()
        self.server_status_label = Tk.Label(self.top, textvariable=self.server_status_value,
                                            width=14, anchor="center")
        self.server_status_label.grid(column=3, row=row, sticky=Tk.W)

        # Manage wheels
        self.connect_wheel_button = {}
        self.disconnect_wheel_button = {}
        self.wheel_status_value = {}
        self.wheel_status_label = {}
        for idx in (1, 2):
            row += 1
            Tk.Label(self.top, text=f"FW-{idx}").grid(column=0, row=row, sticky=Tk.E, padx=5, pady=5)
            self.connect_wheel_button[idx] = Tk.Button(self.top, text="Connect",
                                                       command=partial(self.connect_wheel, idx))
            self.connect_wheel_button[idx].grid(column=1, row=row, padx=5, pady=5)
            self.disconnect_wheel_button[idx] = Tk.Button(self.top, text="Disconnect",
                                                          command=partial(self.disconnect_wheel, idx))
            self.disconnect_wheel_button[idx].grid(column=2, row=row, padx=5)
            self.wheel_status_value[idx] = Tk.StringVar()
            self.wheel_status_label[idx] = Tk.Label(self.top, textvariable=self.wheel_status_value[idx],
                                                    width=14, anchor="center")
            self.wheel_status_label[idx].grid(column=3, row=row, sticky=Tk.W)

        # Manage focuser
        row += 1
        Tk.Label(self.top, text="Focuser"). grid(column=0, row=row, sticky=Tk.E, padx=5, pady=5)
        self.connect_focuser_button = Tk.Button(self.top, text="Connect", command=self.connect_focuser)
        self.connect_focuser_button.grid(column=1, row=row, padx=5, pady=5)
        self.disconnect_focuser_button = Tk.Button(self.top, text="Disconnect", command=self.disconnect_focuser)
        self.disconnect_focuser_button.grid(column=2, row=row, padx=5)
        self.focuser_status_value = Tk.StringVar()
        self.focuser_status_label = Tk.Label(self.top, textvariable=self.focuser_status_value,
                                             width=14, anchor="center")
        self.focuser_status_label.grid(column=3, row=row, sticky=Tk.W)

        # Manage rotator
        row += 1
        Tk.Label(self.top, text="Rotator"). grid(column=0, row=row, sticky=Tk.E, padx=5, pady=5)
        self.connect_rotator_button = Tk.Button(self.top, text="Connect", command=self.connect_rotator)
        self.connect_rotator_button.grid(column=1, row=row, padx=5, pady=5)
        self.disconnect_rotator_button = Tk.Button(self.top, text="Disconnect", command=self.disconnect_rotator)
        self.disconnect_rotator_button.grid(column=2, row=row, padx=5)
        self.rotator_status_value = Tk.StringVar()
        self.rotator_status_label = Tk.Label(self.top, textvariable=self.rotator_status_value,
                                             width=14, anchor="center")
        self.rotator_status_label.grid(column=3, row=row, sticky=Tk.W)

        # Cancel button
        row += 1
        Tk.Button(self.top, text="Close", command=self.onclose).grid(column=2, row=row, columnspan=2, pady=10)

        # Start status updater
        self.window.root.after(250, self.update_statuses)

    def set_camera_status(self, new_status):
        if self.camera_status_value.get() == new_status:
            # Already have this status, exiting
            return
        if new_status == "Connected":
            self.camera_status_value.set("Connected")
            self.camera_status_label.config(fg="green")
            self.connect_camera_button.config(state="disabled")
            self.disconnect_camera_button.config(state="normal")
            return
        elif new_status == "Disconnected":
            self.camera_status_value.set("Disconnected")
            self.camera_status_label.config(fg="red")
            self.connect_camera_button.config(state="normal")
            self.disconnect_camera_button.config(state="disabled")
            return

    def set_server_status(self, new_status):
        if self.server_status_value.get() == new_status:
            # Already have this status, exiting
            return
        if new_status == "Connected":
            self.server_status_value.set("Connected")
            self.server_status_label.config(fg="green")
            self.connect_server_button.config(state="disabled")
            self.disconnect_server_button.config(state="normal")
            return
        elif new_status == "Disconnected":
            self.server_status_value.set("Disconnected")
            self.server_status_label.config(fg="red")
            self.connect_server_button.config(state="normal")
            self.disconnect_server_button.config(state="disabled")

    def set_wheel_status(self, wheel_idx, new_status):
        if self.wheel_status_value[wheel_idx].get() == new_status:
            # Already have this status, exiting
            return
        # Setting new status
        self.wheel_status_value[wheel_idx].set(new_status)
        if new_status in ("N/A", "Unplugged", "No info"):
            self.connect_wheel_button[wheel_idx].config(state="disabled")
            self.disconnect_wheel_button[wheel_idx].config(state="disabled")
            self.wheel_status_label[wheel_idx].config(fg="black")
        elif new_status == "Connected":
            self.connect_wheel_button[wheel_idx].config(state="disabled")
            self.disconnect_wheel_button[wheel_idx].config(state="normal")
            self.wheel_status_label[wheel_idx].config(fg="green")
        elif new_status == "Disconnected":
            self.connect_wheel_button[wheel_idx].config(state="normal")
            self.disconnect_wheel_button[wheel_idx].config(state="disabled")
            self.wheel_status_label[wheel_idx].config(fg="red")

    def set_focuser_status(self, new_status):
        if self.focuser_status_value.get() == new_status:
            # Already have this status, exiting
            return
        # Setting new status
        self.focuser_status_value.set(new_status)
        if new_status == "Connected":
            self.connect_focuser_button.config(state="disabled")
            self.disconnect_focuser_button.config(state="normal")
            self.focuser_status_label.config(fg="green")
        elif new_status == "Disconnected":
            self.connect_focuser_button.config(state="normal")
            self.disconnect_focuser_button.config(state="disabled")
            self.focuser_status_label.config(fg="red")
        elif new_status == "N/A":
            self.connect_focuser_button.config(state="disabled")
            self.disconnect_focuser_button.config(state="disabled")
            self.focuser_status_label.config(fg="black")

    def set_rotator_status(self, new_status):
        if self.rotator_status_value.get() == new_status:
            # Already have this status, exiting
            return
        # Setting new status
        self.rotator_status_value.set(new_status)
        if new_status == "Connected":
            self.connect_rotator_button.config(state="disabled")
            self.disconnect_rotator_button.config(state="normal")
            self.rotator_status_label.config(fg="green")
        elif new_status == "Disconnected":
            self.connect_rotator_button.config(state="normal")
            self.disconnect_rotator_button.config(state="disabled")
            self.rotator_status_label.config(fg="red")
        elif new_status == "N/A":
            self.connect_rotator_button.config(state="disabled")
            self.disconnect_rotator_button.config(state="disabled")
            self.rotator_status_label.config(fg="black")

    def update_statuses(self):
        """
        Check azt onboard server status message and update device statuses accordingly
        """
        if self.is_alive and self.window.alive:
            # Check camera status
            if self.window.gecamera.connected:
                self.set_camera_status("Connected")
            else:
                self.set_camera_status("Disconnected")
            # Check server status
            if self.window.azt.active_connection:
                self.set_server_status("Connected")
                # Check the devices only if the server is connected, as we do not know
                # their statuses otherwise
                for wheel_idx in (1, 2):
                    if (f"wheel_{wheel_idx}" not in self.window.azt.state):
                        # For some reason server status message does not contain any information
                        # about the wheel. This really should not happen, but let's check this
                        # option just in case.
                        self.set_wheel_status(wheel_idx, "No info")
                    else:
                        # There is some information about the wheel 1. Let's see what we got
                        if self.window.azt.state[f"wheel_{wheel_idx}"]["Exists"] is False:
                            # This means that the wheel object file does not exist on the onboard server.
                            # Probably the wheel does not have a power supply or is physically detatched
                            self.set_wheel_status(wheel_idx, "Unplugged")
                        elif self.window.azt.state[f"wheel_{wheel_idx}"]["Connected"] is True:
                            # Wheel exists and connected
                            self.set_wheel_status(wheel_idx, "Connected")
                        elif self.window.azt.state[f"wheel_{wheel_idx}"]["Connected"] is False:
                            # Wheel exists but not connected
                            self.set_wheel_status(wheel_idx, "Disconnected")
                # Focuser status
                if "focuser" not in self.window.azt.state:
                    # No information about focuser in the server message.
                    self.set_focuser_status("No info")
                else:
                    # There is some information about the focuser
                    if self.window.azt.state["focuser"]["Connected"] is True:
                        # Focuser exists and connected
                        self.set_focuser_status("Connected")
                    elif self.window.azt.state["focuser"]["Connected"] is False:
                        # Focuser exists but not connected
                        self.set_focuser_status("Disconnected")
                # Rotator status
                if "rotator" not in self.window.azt.state:
                    # No information about rotator in the server message.
                    self.set_rotator_status("No info")
                else:
                    # There is some information about the rotator
                    if self.window.azt.state["rotator"]["Connected"] is True:
                        # Rotator exists and connected
                        self.set_rotator_status("Connected")
                    elif self.window.azt.state["rotator"]["Connected"] is False:
                        # Rotator exists but not connected
                        self.set_rotator_status("Disconnected")
            else:
                self.set_server_status("Disconnected")
                # We do not know statuses of devices if the server is not connected, so
                # mark them as not available and disable their buttons
                for wheel_idx in (1, 2):
                    self.set_wheel_status(wheel_idx, "N/A")
                self.set_focuser_status("N/A")
                self.set_rotator_status("N/A")
            self.window.root.after(250, self.update_statuses)

    def connect_camera(self):
        self.window.gecamera.keep_connected = True

    def disconnect_camera(self):
        self.window.gecamera.close_connection()

    def connect_server(self):
        self.window.azt.attach()

    def disconnect_server(self):
        self.window.azt.detatch()

    def connect_wheel(self, wheel_idx):
        Thread(target=self.window.devicesPanel.wheel_ids[wheel_idx].connect).start()

    def disconnect_wheel(self, wheel_idx):
        Thread(target=self.window.devicesPanel.wheel_ids[wheel_idx].disconnect).start()

    def connect_focuser(self):
        self.window.azt.send_command("focuser", "connect", ())

    def disconnect_focuser(self):
        self.window.azt.send_command("focuser", "disconnect", ())

    def connect_rotator(self):
        self.window.azt.send_command("rotator", "connect", ())

    def disconnect_rotator(self):
        self.window.azt.send_command("rotator", "disconnect", ())

    def onclose(self):
        self.is_alive = False
        self.window.popup_exists = False
        self.top.destroy()


class SetNextWindow(Tk.Frame):
    """
    The window to configure next exposition parameters
    """
    def __init__(self, window):
        self.window = window
        self.top = Tk.Toplevel(self.window.root)
        self.top.title("Set next exposure")
        self.top.geometry('+%i+%i' % (self.window.root.winfo_x()+80,
                                      self.window.root.winfo_y()+80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.window.popup_exists = True

        # Next photometric filter
        Tk.Label(self.top, text="Next photometric filter:").grid(column=0, row=0, sticky=Tk.E, padx=5, pady=5)
        self.phot_filter_value = Tk.StringVar()
        self.phot_filter_value.set(self.window.devicesPanel.filterwheel_photo_controls.current_filter_name)
        for col, filter_name in enumerate("UBVRI"):
            Tk.Radiobutton(self.top, text=filter_name, variable=self.phot_filter_value,
                           value=filter_name).grid(column=col+1, row=0)

        # Next polarimetric filter
        Tk.Label(self.top, text="Next polarimetric filter:").grid(column=0, row=1, sticky=Tk.E, padx=5, pady=5)
        self.polar_filter_value = Tk.StringVar()
        self.polar_filter_value.set(self.window.devicesPanel.filterwheel_polar_controls.current_filter_name)
        for col, filter_name in enumerate(["X", "Y", "Empty"]):
            Tk.Radiobutton(self.top, text=filter_name, variable=self.polar_filter_value,
                           value=filter_name).grid(column=col+1, row=1)
        # Next exposure value
        Tk.Label(self.top, text="Next exposure: ").grid(column=0, row=2, sticky=Tk.E, padx=5, pady=5)
        self.exposure_value = Tk.StringVar()
        self.exposure_value.set(self.window.grabPanel.exposure_value.get())
        self.exposure_entry = Tk.Entry(self.top, textvariable=self.exposure_value, width=3)
        self.exposure_entry.bind("<Return>", lambda e: self.set_next())
        self.exposure_entry.grid(column=1, row=2)

        # Next count value
        Tk.Label(self.top, text="Next count: ").grid(column=0, row=3, sticky=Tk.E, padx=5, pady=5)
        self.count_value = Tk.StringVar()
        self.count_value.set(self.window.grabPanel.count_value.get())
        self.count_entry = Tk.Entry(self.top, textvariable=self.count_value, width=3)
        self.count_entry.bind("<Return>", lambda e: self.set_next())
        self.count_entry.grid(column=1, row=3)

        # Next starting index
        Tk.Label(self.top, text="Next start idx: ").grid(column=0, row=4, sticky=Tk.E, padx=5, pady=5)
        self.start_index_value = Tk.StringVar()
        self.start_index_value.set(int(self.window.grabPanel.start_idx_value.get()))
        self.start_index_entry = Tk.Entry(self.top, textvariable=self.start_index_value, width=3)
        self.start_index_entry.bind("<Return>", lambda e: self.set_next())
        self.start_index_entry.grid(column=1, row=4)

        # Set button
        Tk.Button(self.top, text="Set", command=self.set_next).grid(column=0, row=5)

        # Reset button
        Tk.Button(self.top, text="Reset", command=self.reset).grid(column=1, row=5)

        # Cancel button
        Tk.Button(self.top, text="Cancel", command=self.onclose).grid(column=2, row=5, columnspan=2)

    def validate(self):
        try:
            exposure = float(self.exposure_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning", message="Wrong exposure value!")
            return False
        if exposure <= 0:
            messagebox.showwarning(parent=self.top, title="Warning", message="Exposure length must be positive!")
            return False
        try:
            count = float(self.count_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning", message="Wrong count value!")
            return False
        if count <= 0:
            messagebox.showwarning(parent=self.top, title="Warning", message="Count value must be positive!")
            return False
        try:
            int(self.start_index_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning",
                                   message="Start index value must be positive integer!")
            return False
        print("Validation true")
        return True

    def set_next(self):
        if self.validate() is False:
            return
        next_exposure_parameters = {}
        next_exposure_parameters["photo"] = self.phot_filter_value.get()
        next_exposure_parameters["polar"] = self.polar_filter_value.get()
        next_exposure_parameters["exposure"] = self.exposure_value.get()
        next_exposure_parameters["count"] = self.count_value.get()
        next_exposure_parameters["index"] = self.start_index_value.get()
        self.window.grabPanel.setup_next_exposure(next_exposure_parameters)
        self.window.statusPanel.set_next_exposure_info(next_exposure_parameters)
        self.onclose()

    def reset(self):
        self.window.grabPanel.setup_next_exposure(None)
        self.window.statusPanel.set_next_exposure_info(None)
        self.onclose()

    def onclose(self):
        self.window.popup_exists = False
        self.top.destroy()
