#! /usr/bin/env python

import tkinter as Tk
import time
import os

if os.name == 'nt':
    PASSIVE_COLOR = "SystemButtonFace"
else:
    PASSIVE_COLOR = "#d9d9d9"


class StatusPanel(Tk.Frame):
    def __init__(self, window, pro_mode=False):
        self.window = window
        self.pro_mode = pro_mode
        self.panel = Tk.LabelFrame(self.window.root, text="Info")
        self.panel.grid(column=1, row=6, sticky=Tk.S+Tk.W+Tk.E, padx=5)
        self.update_info_is_working = False

        # Define some variables to show varoius parameters:
        self.cooler_cold_part_value = Tk.StringVar()
        self.cooler_hot_part_value = Tk.StringVar()
        self.cooler_power_value = Tk.StringVar()
        self.shutter_status_value = Tk.StringVar()
        self.camera_status_value = Tk.StringVar()
        self.onboard_status_value = Tk.StringVar()
        self.intensity_value = Tk.StringVar()
        self.next_filters_value = Tk.StringVar()
        self.next_filters_value.set("not set")
        self.next_exposure_value = Tk.StringVar()
        self.next_exposure_value.set("not set")
        self.next_index_value = Tk.StringVar()
        self.next_index_value.set("not set")
        self.camera_status_value.set("Connecting")
        self.onboard_status_value.set("Connecting")
        self.outside_temperature_value = Tk.StringVar()
        self.outside_temperature_value.set("--")
        if pro_mode:
            self.wheel_1_stepper_position_value = Tk.StringVar()
            self.wheel_2_stepper_position_value = Tk.StringVar()
        # Set the status variable to a camera's local variable, so it can tell
        # the control panel what it is doing now
        self.window.gecamera.camera_status = self.camera_status_value

        # Camera status
        Tk.Label(self.panel, text="Camera:").grid(column=0, row=0, sticky=Tk.W)
        self.camera_status_label = Tk.Label(self.panel, textvariable=self.camera_status_value,
                                            anchor=Tk.W, fg="red", width=20)
        self.camera_status_label.grid(column=1, row=0, sticky=Tk.W)

        # AZT onboard status
        Tk.Label(self.panel, text="Onboard:").grid(column=0, row=1, sticky=Tk.W)
        self.onboard_status_label = Tk.Label(self.panel, textvariable=self.onboard_status_value,
                                             anchor=Tk.W, fg="red", width=20)
        self.onboard_status_label.grid(column=1, row=1, sticky=Tk.W)

        # Cooler status
        Tk.Label(self.panel, text="Cooler:").grid(column=0, row=2, sticky=Tk.W)
        # Cooler status consista of several values, which we would like to control
        # separately, so let's make a new Frame to pack them
        self.cooler_status_panel = Tk.Frame(self.panel)
        self.cooler_status_panel.grid(column=1, row=2, sticky=Tk.W)
        # Cooler cold part label
        self.cooler_cold_part_label = Tk.Label(self.cooler_status_panel, width=4, justify=Tk.RIGHT,
                                               textvariable=self.cooler_cold_part_value)
        self.cooler_cold_part_label.grid(column=0, row=0, sticky=Tk.E)
        # Separator
        Tk.Label(self.cooler_status_panel, text="/").grid(column=1, row=0)
        # Cooler hot part label
        self.cooler_hot_part_label = Tk.Label(self.cooler_status_panel, textvariable=self.cooler_hot_part_value)
        self.cooler_hot_part_label.grid(column=2, row=0)
        # Cooler power label
        self.cooler_power_label = Tk.Label(self.cooler_status_panel, textvariable=self.cooler_power_value)
        self.cooler_power_label.grid(column=3, row=0)

        # Intensity
        Tk.Label(self.panel, text="Pointer:").grid(column=0, row=5, sticky=Tk.W)
        Tk.Label(self.panel, textvariable=self.intensity_value, anchor=Tk.W, width=20).grid(column=1, row=5)

        # Outside temperature measured by focuser-rotator onboard thermometer
        Tk.Label(self.panel, text="T(outside):").grid(column=0, row=6, sticky=Tk.W)
        Tk.Label(self.panel, textvariable=self.outside_temperature_value, anchor=Tk.W, width=20).grid(column=1, row=6)

        # Next exposure parameters
        Tk.Label(self.panel, text="Next filters: ").grid(column=0, row=7, sticky=Tk.W)
        Tk.Label(self.panel, textvariable=self.next_filters_value, anchor=Tk.W).grid(column=1, row=7, sticky=Tk.W)
        Tk.Label(self.panel, text="Next exposure: ").grid(column=0, row=8, sticky=Tk.W)
        Tk.Label(self.panel, textvariable=self.next_exposure_value, anchor=Tk.W).grid(column=1, row=8, sticky=Tk.W)
        # Tk.Label(self.panel, text="Next index: ").grid(column=0, row=8, sticky=Tk.W)
        # Tk.Label(self.panel, textvariable=self.next_index_value, anchor=Tk.W).grid(column=1, row=8, sticky=Tk.W)

        # Pro mode: wheels stepper information
        if pro_mode:
            Tk.Label(self.panel, text="FW-1 step: ").grid(column=0, row=9, sticky=Tk.W)
            Tk.Label(self.panel, textvariable=self.wheel_1_stepper_position_value,
                     anchor=Tk.W).grid(column=1, row=9, sticky=Tk.W)
            Tk.Label(self.panel, text="FW-2 step: ").grid(column=0, row=10, sticky=Tk.W)
            Tk.Label(self.panel, textvariable=self.wheel_2_stepper_position_value,
                     anchor=Tk.W).grid(column=1, row=10, sticky=Tk.W)

    def update_info(self):
        """
        Every two secons fetch the temperature parameters and show them in the status section
        """
        self.update_info_is_working = True
        while 1:
            if self.window.alive is False:
                print("update_info stopped")
                self.update_info_is_working = False
                return
            # Update camera cooler temperatures and power
            cold, hot = self.window.gecamera.temperature_control_get_temperature()
            if (cold is None) or (hot is None):
                time.sleep(2)
                continue
            # If temperatures of power are out of range show them with color, to draw
            # the observers attention
            if self.window.gecamera.temperature_control_is_working:
                if hot < 36:
                    self.cooler_hot_part_label.config(bg=PASSIVE_COLOR)
                else:
                    self.cooler_hot_part_label.config(bg="tomato")
                if self.window.gecamera.current_set_temperature is not None:
                    if abs(self.window.gecamera.current_set_temperature - cold) < 0.5:
                        self.cooler_cold_part_label.config(bg=PASSIVE_COLOR)
                    elif 1 < abs(self.window.gecamera.current_set_temperature - cold) < 1:
                        self.cooler_cold_part_label.config(bg="gold")
                    else:
                        self.cooler_cold_part_label.config(bg="tomato")
                self.cooler_cold_part_value.set("%-2.1f" % cold)
                self.cooler_hot_part_value.set("%1.1f" % hot)
            # Update outside temperature
            temp = self.window.devicesPanel.focuser_rotator_controls.focuser_temperature
            if temp is not None:
                string = "%1.1f\u00B0" % temp
                self.outside_temperature_value.set(string)
            # Update wheels stepper info
            if self.pro_mode:
                pos = self.window.devicesPanel.filterwheel_photo_controls.get_info("Stepper_pos")
                self.wheel_1_stepper_position_value.set(str(pos))
                pos = self.window.devicesPanel.filterwheel_polar_controls.get_info("Stepper_pos")
                self.wheel_2_stepper_position_value.set(str(pos))
            time.sleep(2)

    def set_next_exposure_info(self, next_exposure_parameters=None):
        if next_exposure_parameters is None:
            filters_string = 'not set'
            expo_string = 'not set'
        else:
            filters_string = "%s/%s" % (next_exposure_parameters['photo'],
                                        next_exposure_parameters['polar'])
            expo_string = "time=%s count=%s idx=%s" % (next_exposure_parameters['exposure'],
                                                       next_exposure_parameters['count'],
                                                       next_exposure_parameters['index'])

        self.next_filters_value.set(filters_string)
        self.next_exposure_value.set(expo_string)
