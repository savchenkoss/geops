#! /usr/bin/env python

import tkinter as Tk
from tkinter import messagebox
import time
from threading import Thread
import os


ACTIVE_BUTTON_COLOR = "spring green"
if os.name == 'nt':
    PASSIVE_BUTTON_COLOR = "SystemButtonFace"
else:
    PASSIVE_BUTTON_COLOR = "#d9d9d9"


class FLIWheelGUI(Tk.Frame):
    """
    GUI for FLI filter wheel
    """
    def __init__(self, focus_wheel_panel, label, server, device, filter_names, default_filter=0,
                 pro_mode=False, delay=1000):
        self.window = focus_wheel_panel.window
        self.server = server
        self.device = device
        self.delay = delay
        self.label = label
        # If in_action is true, this means that the wheel is doing something and when this
        # action is done, it will automatically update the status. This flag should prevent
        # external calls to change the wheel status while it is doing something
        self.in_action = False
        # Wheel device can have following statuses:
        # Idle -- when connected, not moving, ready to move
        # Connecting -- when trying to connect
        # Disconnecting -- command disconnect just sent
        # Disconnected -- when not connected (initial state)
        # Moving -- when moving to new position
        # Homing -- when homing
        self.status = "Disconnected"
        self.status_message = Tk.StringVar()
        self.status_message.set("Disconnected")
        self.filter_names = filter_names
        self.default_filter = default_filter
        self.locked = True
        self.panel = Tk.Frame(focus_wheel_panel.panel)
        self.panel.pack(anchor=Tk.W)
        Tk.Label(self.panel, text=f"{label}:", font=("Times", 12), width=7,
                 justify=Tk.LEFT).grid(column=0, row=0, sticky=Tk.W)
        # Create buttons
        self.buttons = []
        self.buttons_panel = Tk.Frame(self.panel)
        self.buttons_panel.grid(column=1, row=0)
        for idx, filter_name in enumerate(self.filter_names):
            button = Tk.Button(self.buttons_panel, text=filter_name, font=("Times", 10, "bold"),
                               command=lambda x=idx: self.on_click(x), state='disabled')
            self.buttons.append(button)
            self.buttons[idx].grid(column=idx+1, row=0, padx=1)
        # Buttons to make steps
        if pro_mode:
            button = Tk.Button(self.buttons_panel, text="-", font=("Times", 10, "bold"),
                               command=lambda x=-1: self.step_motor_click(x), state='disabled')
            self.buttons.append(button)
            self.buttons[-1].grid(column=len(self.buttons)+1, row=0, padx=1)
            button = Tk.Button(self.buttons_panel, text="+", font=("Times", 10, "bold"),
                               command=lambda x=1: self.step_motor_click(x), state='disabled')
            self.buttons.append(button)
            self.buttons[-1].grid(column=len(self.buttons)+1, row=0, padx=1)

        self.buttons_panel.configure(width=100)
        # Create a status panel
        self.status_label = Tk.Label(self.panel, textvariable=self.status_message,
                                     font=("Times", 10), justify=Tk.RIGHT, width=12)
        self.status_label.grid(column=2, row=0, columnspan=1, sticky=Tk.E, padx=2)
        self.current_filter_idx = -1
        self.current_filter_name = "NA"

    def set_status(self, new_status, force=False):
        if (new_status == self.status):
            return
        if (self.in_action is True) and (force is False):
            return
        self.status = new_status
        self.window.root.after(10, self.status_message.set, new_status)
        if new_status == "Idle":
            if not self.window.grabPanel.grab_lock_on:
                self.unlock()
            self.status_label.config(fg="black")
            return
        if new_status == "Connecting":
            self.lock()
            self.status_label.config(fg="green")
            return
        if new_status in ("Disconnected", "Disconnecting", "N/A"):
            self.lock()
            self.status_label.config(fg="red")
        if new_status in ("Moving", "Homing", "Stepping"):
            self.lock()
            self.status_label.config(fg="blue")

    def lock(self):
        """
        Lock all buttons
        """
        if self.locked:
            # Already locked
            return
        self.locked = True
        for button in self.buttons:
            button.config(state="disabled")

    def unlock(self):
        """
        Unlock all buttons
        """
        if not self.locked:
            # Already unlocked
            return
        self.locked = False
        for button in self.buttons:
            button.config(state="normal")

    def get_info(self, parameter):
        try:
            return self.server.state[self.device][parameter]
        except KeyError:
            return None

    def connect(self):
        """
        Connect to the device
        """
        self.set_status("Connecting")
        self.in_action = True
        # First thing, we need to make sure, that the server sees the device, before
        # trying to send the "connect" command to the wheel
        for attempt in range(10):
            if not self.window.alive:
                # User has closed the window while wating for wheel to appear on server
                self.in_action = False
                return
            if self.get_info("Exists") is True:
                # The server sees the device, so we can proceed to the connection
                break
            time.sleep(0.5)
        else:
            # Server do not see the device, so we can not connect. Stop attepts.
            self.in_action = False
            self.set_status("Disconnected")
            return
        # Now, when we are sure, that the device do exists physically, we can try
        # to connect it by sending the appropriate command to the server
        attempt = 10
        while attempt > 0:
            self.server.send_command(self.device, "connect", ())
            for _ in range(20):
                time.sleep(0.5)
                if not self.window.alive:
                    # Programm was closed before the device was connected
                    self.in_action = False
                    return
                if self.get_info("Connected") is True:
                    # Wheel is connected
                    attempt = 0
                    break
            attempt -= 1
        if self.get_info("Connected") is False:
            # We have exhausted all attempts but still not connected. Update state and give up
            self.in_action = False
            self.set_status("Disconnected")
            return
        # Wheel should start homing at this moment
        self.set_status("Homing", force=True)
        while self.window.alive:
            # Wait for homing
            pos = self.get_info("Filter_pos")
            if (self.get_info("Moving") is False) and ((pos is not None) and (pos != -1)):
                break
            time.sleep(0.1)
        else:
            # Window was closed while waiting for homing to end
            self.in_action = False
            return
        self.select_filter(self.default_filter)
        self.in_action = False
        self.set_status("Idle")

    def disconnect(self):
        self.set_status("Disconnecting")
        self.in_action = True
        for attempt in range(10):
            self.server.send_command(self.device, "disconnect", ())
            for _ in range(10):
                time.sleep(0.5)
                if self.get_info("Connected") is False:
                    self.in_action = False
                    self.set_status("Disconnected")
                    return

    def on_click(self, filter_name):
        # When the user switches the filter, ze probably wants to start a
        # new sequence from so we can set starting index in the
        # grab panel equal to zero
        self.window.grabPanel.start_idx_value.set("0")
        # Start filter switching in the background
        # self.window.root.after(10, self.select_filter, filter_name)
        Thread(target=self.select_filter, args=(filter_name,)).start()

    def select_filter(self, filter_idx):
        print("wheel", self.label, "setting", filter_idx)
        if isinstance(filter_idx, str):
            filter_idx = self.filter_names.index(filter_idx)
        if filter_idx == self.current_filter_idx:
            return
        self.in_action = True
        self.set_status("Moving", force=True)
        self.server.send_command(self.device, "set_filter_pos", (filter_idx, ))

        while self.window.alive:
            time.sleep(0.25)
            if self.get_info("Filter_pos") is None:
                continue
            if self.get_info("Filter_pos") != self.current_filter_idx:
                self.buttons[self.current_filter_idx].config(bg=PASSIVE_BUTTON_COLOR)
                self.current_filter_idx = self.get_info("Filter_pos")
                self.current_filter_name = self.filter_names[self.current_filter_idx]
                self.buttons[self.current_filter_idx].config(bg=ACTIVE_BUTTON_COLOR)
                break
        self.in_action = False
        self.set_status("Idle")

    def step_motor_click(self, step):
        Thread(target=self.step_motor, args=(step, )).start()

    def step_motor(self, step):
        self.set_status("Stepping")
        self.in_action = True
        self.server.send_command(self.device, "step_motor", (step,))
        time.sleep(0.5)
        self.in_action = False
        self.set_status("Idle")


class FocuserRotatorGUI(Tk.Frame):
    """
    GUI for the fucuser rotator device
    """
    def __init__(self, focus_wheel_panel, server):
        self.window = focus_wheel_panel.window
        self.server = server
        self.focuser_locked = True
        self.rotator_locked = True
        self.focuser_connect_time = None
        self.rotator_connect_time = None
        # The main panel
        self.panel = Tk.Frame(focus_wheel_panel.panel)
        self.panel.pack(anchor=Tk.W, pady=5)

        # Focuser GUI
        Tk.Label(self.panel, text="Focus:", font=("Times", 12), width=7,
                 justify=Tk.LEFT).grid(column=0, row=0, sticky=Tk.W)
        # Go left button
        self.go_left_button = Tk.Button(self.panel, text="\u2212", font=("Times", 13, "bold"),
                                        command=self.focuser_go_in, state="disabled")
        self.go_left_button.grid(column=1, row=0, padx=1)
        # Step entry
        self.step_variable = Tk.StringVar()
        self.step_variable.set(10)
        self.move_step_entry = Tk.Entry(self.panel, textvariable=self.step_variable,
                                        width=6, justify=Tk.CENTER, state="disabled")
        self.move_step_entry.grid(column=2, row=0, padx=1)
        # Go right button
        self.go_right_button = Tk.Button(self.panel, text="+", font=("Times", 13, "bold"),
                                         command=self.focuser_go_out, state="disabled")
        self.go_right_button.grid(column=3, row=0, padx=1)
        # Current value
        self.focuser_position_value = Tk.StringVar()
        self.focuser_position_value.set("---")
        self.focuser_position_label = Tk.Label(self.panel, textvariable=self.focuser_position_value,
                                               font=("monospace", 12), justify=Tk.CENTER, state="disabled")
        self.focuser_position_label.grid(column=4, row=0, padx=10)
        # Second row of buttons is in different layout
        self.second_row = Tk.Frame(self.panel)
        self.second_row.grid(column=1, row=1, columnspan=4, sticky=Tk.W)

        # Rotator GUI
        Tk.Label(self.panel, text="Rotator:", font=("Times", 12), width=7,
                 justify=Tk.LEFT).grid(column=0, row=2, sticky=Tk.W, pady=9)
        self.angle_value = Tk.StringVar()
        self.angle_value.set(0)
        self.angle_entry = Tk.Entry(self.panel, textvariable=self.angle_value, width=5,
                                    justify=Tk.CENTER, state="disabled")
        self.angle_entry.grid(column=1, row=2)
        self.angle_entry.bind("<Return>", lambda e: self.toggle_rotation())
        # Rotate button. The behaviour of this button depends on the current state of the
        # rotator. If the rotator is idling now, the buttons starts the rotation. If the
        # rotation is in process the buttons stopps the rotations
        self.toggle_rotation_button = Tk.Button(self.panel, text="Rotate", width=8,
                                                command=self.toggle_rotation, state="disabled")
        self.toggle_rotation_button.grid(column=2, row=2, columnspan=2)
        # Current angle
        self.rotator_position_display_value = Tk.StringVar()
        self.rotator_position_display_value.set(0)
        self.rotator_position_degrees = 0
        self.rotator_position_label = Tk.Label(self.panel, textvariable=self.rotator_position_display_value,
                                               font=("monospace", 12), justify=Tk.CENTER, state="disabled")
        self.rotator_position_label.grid(column=4, row=2)

        # Default devices parameters
        self.focuser_position = None
        self.focuser_temperature = None
        self.focuser_moving = None
        self.waiting_for_stop_moving = False
        self.rotator_position_degrees = None
        self.rotator_moving = None

        # Wait for the devices to connect
        # self.window.root.after(500, self.wait_focuser)
        # self.window.root.after(500, self.wait_rotator)
        # Thread(target=self.wait_focuser).start()
        # Thread(target=self.wait_rotator).start()

    def wait_focuser(self):
        """
        Function waits utill the focuser is ready to use and then
        enables the GUI
        """
        if not self.window.alive:
            return
        if "focuser" not in self.server.state:
            self.window.root.after(500, self.wait_focuser)
            return
        # Check if device connected
        if self.server.state["focuser"]["Connected"] is True:
            print("Focuser connected")
            self.waiting_for_stop_moving = False
            # If so, move forward, if not connected in 5 seconds, the command will be sent again
            self.window.root.after(10, self.focuser_info_updater)
            self.window.root.after(10, self.focuser_unlock)
            return
        # Connect to the device
        # Send 'connect' command
        if (self.focuser_connect_time is None) or (time.time() - self.focuser_connect_time > 5):
            self.server.send_command("focuser", "connect", ())
            self.focuser_connect_time = time.time()
        self.window.root.after(500, self.wait_focuser)

    def wait_rotator(self):
        """
        Function waits utill the rotator is ready to use and then
        enables the GUI
        """
        if not self.window.alive:
            return
        if "rotator" not in self.server.state:
            self.window.root.after(500, self.wait_rotator)
            return
        # Check if device connected
        if self.server.state["rotator"]["Connected"] is True:
            print("Rotator connected")
            # If so, move forward, if not connected in 5 seconds, the command will be sent again
            self.window.root.after(10, self.rotator_info_updater)
            self.window.root.after(10, self.rotator_unlock)
            return
        # Connect to the device
        # Send 'connect' command
        if (self.rotator_connect_time is None) or (time.time() - self.rotator_connect_time > 5):
            self.server.send_command("rotator", "connect", ())
            self.rotator_connect_time = time.time()
        self.window.root.after(500, self.wait_rotator)

    def focuser_lock(self):
        if self.focuser_locked:
            # Already locked
            return
        self.focuser_locked = True
        self.go_left_button.config(state="disabled")
        self.move_step_entry.config(state="disabled")
        self.go_right_button.config(state="disabled")
        self.focuser_position_label.config(state="disabled")

    def focuser_unlock(self):
        if not self.focuser_locked:
            # Already unlocked
            return
        self.focuser_locked = False
        self.go_left_button.config(state="normal")
        self.move_step_entry.config(state="normal")
        self.go_right_button.config(state="normal")
        self.focuser_position_label.config(state="normal")

    def focuser_unlock_when_ready(self):
        """
        Wait for the focuser to finish moving and unlock it's GUI
        """
        self.waiting_for_stop_moving = True
        time.sleep(1.75)
        while self.focuser_moving is not False:
            time.sleep(0.1)
        self.waiting_for_stop_moving = False
        self.focuser_unlock()

    def rotator_lock(self):
        if self.rotator_locked:
            # Already locked
            return
        self.rotator_locked = True
        self.angle_entry.config(state="disabled")
        self.toggle_rotation_button.config(state="disabled")
        self.rotator_position_label.config(state="disabled")

    def rotator_unlock(self):
        if not self.rotator_locked:
            # Already unlocked
            return
        self.rotator_locked = False
        self.angle_entry.config(state="normal")
        self.toggle_rotation_button.config(state="normal")
        self.rotator_position_label.config(state="normal")

    def lock(self):
        self.focuser_lock()
        self.rotator_lock()

    def unlock(self):
        self.focuser_unlock()
        self.rotator_unlock()

    def focuser_info_updater(self):
        """
        Reads the focuser parameters from the server and updates the
        application status
        """
        if self.window.alive is False:
            # Stop updating when the main window is closed
            return
        if "focuser" not in self.server.state:
            self.window.root.after(250, self.focuser_info_updater)
            return
        if "Position" in self.server.state["focuser"]:
            self.focuser_position = self.server.state["focuser"]["Position"]
            if self.focuser_position is not None:
                self.focuser_position_value.set(int(self.focuser_position))
            else:
                self.focuser_position_value.set(-1)
        if "Temperature" in self.server.state["focuser"]:
            self.focuser_temperature = self.server.state["focuser"]["Temperature"]
        if "Moving" in self.server.state["focuser"]:
            self.focuser_moving = self.server.state["focuser"]["Moving"]
        self.window.root.after(250, self.focuser_info_updater)

    def rotator_info_updater(self):
        if self.window.alive is False:
            # Stop updating when the main window is closed
            return
        if "rotator" not in self.server.state:
            self.window.root.after(250, self.rotator_info_updater)
            return
        # Take zero point into account and show the user 'clean' instrumental pa value
        if "Posang" in self.server.state["rotator"]:
            current_pa = self.server.state["rotator"]["Posang"]
        else:
            current_pa = None
        if current_pa is not None:
            offset = float(self.window.config_params.params["rotator_zero_point"])
            self.rotator_position_degrees = int(round(current_pa - offset, 0))
            self.rotator_position_display_value.set(str(self.rotator_position_degrees) + "\u00B0")
        if "Moving" in self.server.state["rotator"]:
            self.rotator_moving = self.server.state["rotator"]["Moving"]
        self.window.root.after(250, self.rotator_info_updater)

    def validate_step(self):
        try:
            shift = int(self.step_variable.get())
        except ValueError:
            messagebox.showwarning(parent=self.panel, title="Warning",
                                   message="Wrong focus step value!")
            return False
        if shift <= 0:
            messagebox.showwarning(parent=self.panel, title="Warning",
                                   message="Focus step value must be positive!")
            return False
        return True

    def focuser_go_in(self):
        if self.validate_step() is False:
            return
        shift = int(self.step_variable.get())
        self.server.send_command("focuser", "move_inward", (shift,))
        self.focuser_lock()
        Thread(target=self.focuser_unlock_when_ready).start()

    def focuser_go_out(self):
        if self.validate_step() is False:
            return
        shift = int(self.step_variable.get())
        self.server.send_command("focuser", "move_outward", (shift,))
        self.focuser_lock()
        Thread(target=self.focuser_unlock_when_ready).start()

    def toggle_rotation(self):
        if self.rotator_moving is False:
            # Rotator is not moving. Run movement starting procedure
            try:
                posang = int(self.angle_value.get())
            except ValueError:
                messagebox.showwarning(parent=self.panel, title="Warning",
                                       message="Wrong PosAng value!")
                return False
            if not (-90 < posang < 90):
                messagebox.showwarning(parent=self.panel, title="Warning",
                                       message="Position angle value must be between -90 and 90!")
                return

            new_posang = (int(self.angle_value.get()) +
                          float(self.window.config_params.params["rotator_zero_point"]))
            # if abs(new_posang - self.rotator_position_degrees) < 100:
            #     return
            self.server.send_command("rotator", "move_abs_posang", (new_posang,))
            self.toggle_rotation_button.config(state="disabled")
            self.toggle_rotation_button.config(text="Rotating")
            self.rotator_lock()

            # Wait in a background untill the rotation is finished to change the button name back
            def f():
                time.sleep(2)
                while self.rotator_moving is True:
                    time.sleep(0.1)
                self.toggle_rotation_button.config(state="normal")
                self.toggle_rotation_button.config(text="Rotate")
                self.rotator_unlock()
            Thread(target=f).start()

        else:
            # Rotator is already moving: do nothing, as this rotator can't be stopped
            pass


class DevicesPanel(Tk.Frame):
    """
    A Panel to control filter wheels, focuser and rotator
    """
    def __init__(self, window, pro_mode):
        self.window = window
        self.panel = Tk.LabelFrame(self.window.root, text="Filters and focus control")
        self.panel.grid(column=1, row=0, rowspan=3, sticky=Tk.N+Tk.W+Tk.E, padx=5)

        # FLI wheel 1 controls
        self.filterwheel_photo_controls = FLIWheelGUI(self, label="FW-1", server=self.window.azt, device="wheel_1",
                                                      filter_names=["U", "B", "V", "R", "I"],
                                                      pro_mode=pro_mode, delay=2000)
        # FLI wheel 2 controls
        self.filterwheel_polar_controls = FLIWheelGUI(self, label="FW-2", server=self.window.azt, device="wheel_2",
                                                      filter_names=["X", "Y", "Empty"], default_filter=0,
                                                      pro_mode=pro_mode, delay=2000)
        self.wheel_ids = {1: self.filterwheel_photo_controls,
                          2: self.filterwheel_polar_controls}
        # Focuser-rotator interface
        self.focuser_rotator_controls = FocuserRotatorGUI(self, server=self.window.azt)

    def lock(self):
        self.filterwheel_photo_controls.lock()
        self.filterwheel_polar_controls.lock()
        self.focuser_rotator_controls.lock()

    def unlock(self):
        self.filterwheel_photo_controls.unlock()
        self.filterwheel_polar_controls.unlock()
        self.focuser_rotator_controls.unlock()

    def connect_wheels(self):
        Thread(target=self.filterwheel_photo_controls.connect).start()
        Thread(target=self.filterwheel_polar_controls.connect).start()
