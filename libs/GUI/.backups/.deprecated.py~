class CFW8GUI(Tk.Frame):
    """
    GUI for controlling CFW8 wheel device
    """
    def __init__(self, focus_wheel_panel):
        self.window = focus_wheel_panel.window
        self.cfw8_panel = Tk.Frame(focus_wheel_panel.panel)
        self.cfw8_panel.grid(column=0, row=0, padx=5, pady=3)
        Tk.Label(self.cfw8_panel, text="CFW-8 wheel",
                 font=("Times", 12, "bold")).grid(column=0, row=0, columnspan=2, sticky=Tk.W)
        self.status_message = Tk.StringVar()
        self.status_message.set("Starting")
        Tk.Label(self.cfw8_panel, textvariable=self.status_message, font=("Times", 10),
                 justify=Tk.LEFT).grid(column=2, row=0, columnspan=2, sticky=Tk.W)

        # Buttons
        self.buttons = {}
        for idx, filter_name in enumerate(["U", "B", "V", "R", "I"]):
            button = Tk.Button(self.cfw8_panel, text=filter_name, width=3,
                               font=("Times", 12, "bold"), state='disabled',
                               command=lambda x=filter_name: self.on_click(x))
            self.buttons[filter_name] = button
            self.buttons[filter_name].grid(column=idx, row=1, padx=5)

        # The CFW8 GUI is created with disabled because it is possible that the
        # device is not ready at this moment. In order not to lock the rest of the
        # GUI creation, we will wait for the device in the background and then
        # unlock the buttons
        Thread(target=self.wait).start()

    def wait(self):
        """
        Function waits untill the device is ready to use and then
        enables the GUI
        """
        while 1:
            if self.window.cfw8.is_ready:
                break
            time.sleep(0.05)
        self.buttons[self.window.cfw8.current_filter].config(bg=ACTIVE_BUTTON_COLOR)
        self.status_message.set("Ready")
        self.unlock()

    def lock(self):
        """
        Lock all buttons
        """
        for button in self.buttons.values():
            button.config(state="disabled")

    def unlock(self):
        """
        Unlock all buttons
        """
        for button in self.buttons.values():
            button.config(state="normal")

    def on_click(self, filter_name):
        # When the user switches the filter, ze probably wants to start a
        # new sequence from so we can set starting index in the
        # grab panel equal to zero
        self.window.grabPanel.start_idx_value.set("0")
        # Start filter switching in a background thread
        Thread(target=self.select_filter, args=(filter_name,)).start()

    def select_filter(self, filter_name):
        self.status_message.set("Moving")
        self.lock()
        self.buttons[self.window.cfw8.current_filter].config(bg=PASSIVE_BUTTON_COLOR)
        self.window.cfw8.set_filter(filter_name)
        self.buttons[self.window.cfw8.current_filter].config(bg=ACTIVE_BUTTON_COLOR)
        self.unlock()
        self.status_message.set("Ready")
