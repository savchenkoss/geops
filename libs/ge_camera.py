#! /usr/bin/env python

import ping3
from ctypes import c_char_p
from ctypes import c_int
from ctypes import c_bool
from ctypes import byref
from ctypes import c_uint16
from ctypes import c_uint32
from ctypes import POINTER
import time
from datetime import datetime
from threading import Thread
import numpy as np
from . import ge_lib


class GECamera:
    # Load the DLL
    def __init__(self, cam_addr, ether_addr):
        self.library = ge_lib.GELibrary()
        self.cam_addr = c_int(cam_addr)
        self.ether_addr = ether_addr
        self.model_id = None
        self.model_str = None
        self.connected = False
        self.connecting = False
        self.keep_connected = True
        self.current_interface_type = None
        self.min_temperature = None
        self.max_temperature = None
        self.current_set_temperature = None
        self.current_frametype = None
        self.current_mode_string = ""
        self.temperature_control_is_working = False
        self.temperature_ccd_last_measured = None
        self.temperature_tec_last_measured = None
        self.current_binning = 1
        self.readout_timings_bin1 = {"50_kHz": 22.2,
                                     "100_kHz": 11.3,
                                     "250_kHz": 4.75,
                                     "500_kHz": 2.6,
                                     "1_MHz": 1.25,
                                     "3_MHz": 0.5}
        self.readout_timings_bin2 = {"50_kHz": 11.2,
                                     "100_kHz": 5.7,
                                     "250_kHz": 2.4,
                                     "500_kHz": 1.3,
                                     "1_MHz": 0.7,
                                     "3_MHz": 0.3}
        self.current_readout_timing = None

    def setup_camera_interface(self, iface_type="USB", ip_address=None):
        """
        iface_type: USB or Ethernet
        ip_address: only if iface_tape == Ethernet
        cam_addr: 0...3
        """
        status_msg = c_int(0)
        if iface_type == "USB":
            # Set up usb interface
            ip_address = c_char_p("".encode("UTF8"))
            res = self.library.dll.SetupCameraInterface(ge_lib.connectionType_USB, ip_address,
                                                        byref(status_msg), self.cam_addr)
        elif iface_type == "Ethernet":
            # Set up ethernet interface
            res = self.library.dll.SetupCameraInterface(ge_lib.connectionType_Ethernet, ip_address.encode("UTF8"),
                                                        byref(status_msg), self.cam_addr)
        else:
            # Wrong interface type was given
            print("Error (setup_camera_interace): wrong interface type")
            return False
        # Check the result
        if res is True:
            # Interface set up correctly
            self.current_interface_type = iface_type
            return True
        else:
            # Interface was not set up correctly. Let's check the status message to know why
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (setup_camera_interface): {msg}")
            return False

    def connect_to_single_camera_server(self):
        res = self.library.dll.ConnectToSingleCameraServer(self.cam_addr)
        # Check the result
        if res is True:
            # Connected to server
            print("Connected to server")
            return True
        else:
            # Interface was not set up correctly. Let's check the status message to know why
            print("Error (connect_to_single_camera_server)")
            return False

    def disconnect_camera_server(self):
        res = self.library.dll.DisconnectCameraServer(self.cam_addr)
        # Check the result
        if res is True:
            # Connected to server
            print("Disconnected from server")
            return True
        else:
            # Interface was not set up correctly. Let's check the status message to know why
            print("Error (disconnect_camera_server)")
            return False

    def connect_camera(self):
        """
        Connect to the camera with a given addres
        """
        model_id = c_int(0)
        model_str = c_char_p("".encode("UTF8"))
        status_msg = c_int(0)
        res = self.library.dll.ConnectCamera(byref(model_id), byref(model_str), byref(status_msg), self.cam_addr)
        if res is True:
            # Camera was connected successfully
            self.model_id = model_id.value
            self.model_str = model_str.value
            self.connected = True
            self.keep_connected = True
            print(f"Camera ID: {self.model_id} model {self.model_str} connected")
            return True
        else:
            # Camera was not connected
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (connect_camera): {msg}")
            return False

    def disconnect_camera(self):
        """
        Disconnect a specified camera
        """
        status_msg = c_int(0)
        res = self.library.dll.DisconnectCamera(byref(status_msg), self.cam_addr)
        if res is True:
            # Camera was disconnected successfully
            self.model_id = None
            self.model_str = None
            self.connected = False
            self.keep_connected = False
            print("Camera disconnected")
            return True
        else:
            # Camera was not connected
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (disconnect_camera): {msg}")
            return False

    def init_camera(self):
        """
        Initialize the camera (once right after connection)
        """
        status_msg = c_int(0)
        res = self.library.dll.InitCamera(byref(status_msg), self.cam_addr)
        if res is True:
            # Camera was initialized successfully
            print("Camera initialized")
        else:
            # Camera was not initialized
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (initialize camera): {msg}")
            return False
        # Make the shutter to listen to internal camera trigger and automatically
        # open when neccessary
        res = self.library.dll.OpenShutter(c_int(2), byref(status_msg), self.cam_addr)
        if res is True:
            print("Shutter initialized to by synced with the camera")
        else:
            # Camera was not initialized
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (initialize shutter): {msg}")
            return False
        # Set correct bits per pixel value
        res = self.library.dll.SetBitDepth(c_int(4), byref(status_msg), self.cam_addr)
        if res is True:
            print("Bits per pixel set to 32")
        else:
            # Camera was not initialized
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (setBitDepth): {msg}")
            return False

        # Init temperature control
        res = self.temperature_control_init()
        if res is False:
            return res

        # Set default freqiency mode
        res = self.set_frequency_mode("1_MHz")
        if res is False:
            return res

    def connect_via_usb(self):
        """
        Connect to the camera via usb: setup interface, get number of cameras, connect, initialize
        """
        # Set up interface
        if self.setup_camera_interface("USB") is False:
            return False
        # Get the number of connected cameras
        n_cams = self.get_num_of_devices()
        if n_cams == 0:
            # No cameras found
            print("Error (connect_via_usb): no cameras found.")
            return False
        # Connect the camera
        if self.connect_camera() is False:
            # Connection failed
            return False
        # Initialize the camera
        if self.init_camera() is False:
            # Initialization failed. Disconnect the camera
            self.close_connection()
            return False
        # Setup output mode
        if self.setup_output_mode() is False:
            # Failed
            self.close_connection()
            return False
        return True

    def get_num_of_devices(self):
        return self.library.dll.GetNumberOfConnectedCams()

    def connect_via_ethernet(self):
        """
        Connect to the camera via ethernet: setup, connect to server, connect, initialize
        """
        # time.sleep(4)
        # self.connected = True
        # return
        # Set up interface
        print("Connecting to camera via ethernet")
        self.connecting = True
        if self.setup_camera_interface("Ethernet", self.ether_addr) is False:
            self.connecting = False
            return False
        # Connect to camera server
        if self.connect_to_single_camera_server() is False:
            print("Error (connect_via_ethernet): no cameras found.")
        # Connect the camera
        if self.connect_camera() is False:
            # Connection failed
            self.disconnect_camera_server()
            self.connecting = False
            return False
        # Initialize the camera
        if self.init_camera() is False:
            # Initialization failed. Disconnect the camera
            self.close_connection()
            self.connecting = False
            return False
        # Setup output mode
        if self.setup_output_mode() is False:
            # Failed
            self.close_connection()
            self.connecting = False
            return False
        Thread(target=self.pinger).start()
        print("Connected via ethernet")
        self.connecting = False
        return True

    def close_connection(self):
        """
        Properly close the connection based on the interface type
        """
        print("Camera: closing connection")
        self.disconnect_camera()
        if self.current_interface_type == "Ethernet":
            self.disconnect_camera_server()

    def pinger(self):
        while self.connected:
            ping_time = ping3.ping(self.ether_addr, timeout=5)
            print(f"{ping_time=}")
            current_date_and_time = datetime.now()
            if (ping_time is None) or (ping_time > 0.01):
                current_date_and_time = datetime.now()
                with open("ping_log.dat", "a") as fout:
                    fout.write(f"{current_date_and_time}  {ping_time}\n")
            time.sleep(1)

    def dll_is_busy(self):
        """
        Check if dll is busy now
        """
        return self.library.dll.DllIsBusy(self.cam_addr)

    # Camera configuration
    def set_frequency_mode(self, readout_speed):
        """
        Set readout speed of the camera
        """
        if readout_speed not in ge_lib.readout_speeds.keys():
            print("Wrong readout speed. Possible values are", list(ge_lib.readout_speeds.keys()))
            return False
        status_msg = c_int()
        res = self.library.dll.SetReadOutSpeed(ge_lib.readout_speeds[readout_speed],
                                               byref(status_msg), self.cam_addr)
        if res is True:
            # New readout speed set correctly
            print(f"Readout speed is {readout_speed} now")
            self.current_mode_string = readout_speed
            if self.current_binning == 1:
                self.current_readout_timing = self.readout_timings_bin1[readout_speed]
            else:
                self.current_readout_timing = self.readout_timings_bin2[readout_speed]
            return True
        else:
            # Camera was not initialized
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (set_readout_speed): {msg}")
            return False

    def set_binning(self, binning):
        """
        Set camera binning. bigging=0 for no binning, 1 -- for 2x2 pixels binning,
        2 -- for 4x4 pixels binning and so forth up to binning=7 for 128x128
        """
        if not (0 <= binning <= 7):
            print("Binning must be from 0 to 7")
            return False
        status_msg = c_int()
        res = self.library.dll.SetBinningMode(c_int(binning), c_int(binning), byref(status_msg), self.cam_addr)
        if res is True:
            # New readout speed set correctly
            print(f"The camera binning is {binning} now")
            self.current_binning = binning
            # Readout depends on the binning
            if self.current_binning == 1:
                self.current_readout_timing = self.readout_timings_bin1[self.current_mode_string]
            else:
                self.current_readout_timing = self.readout_timings_bin2[self.current_mode_string]
            return True
        else:
            # Camera was not initialized
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (set_binning): {msg}")
            return False

    def setup_gain(self, gain_setting):
        """
        Setup gain setting: 0 -- maximum dynamic range
                            1 -- highest sensitivity
        """
        status_msg = c_int()
        res = self.library.dll.SetupGain(c_int(gain_setting), byref(status_msg), self.cam_addr)
        if res is True:
            # Shutter opened successfully
            print(f"The gain setting is {gain_setting} now")
            return True
        else:
            # The shutter was not closed for some reason
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (setup_gain): {msg}")
            return False

    def setup_output_mode(self):
        """
        Set the output mode to use a single amplifier and reginser
        """
        res = self.library.dll.SetupSensorOutputMode(c_int(1), self.cam_addr)
        if res is True:
            # Shutter opened successfully
            print("Output mode set up to one ampl.")
            return True
        else:
            # The shutter was not closed for some reason
            print("Error (setup_output_mode)")
            return False

    ###
    # Temperature control
    ###
    def temperature_control_init(self):
        min_temperature = c_int()
        max_temperature = c_int()
        status_msg = c_int()
        cooling_hardware = c_int(42223)
        res = self.library.dll.TemperatureControl_Init(cooling_hardware, byref(min_temperature),
                                                       byref(max_temperature), byref(status_msg),
                                                       self.cam_addr)
        if res >= 0:
            # Temperature control initialized successfully
            print("The temperature control is initialized now")
            self.min_temperature = min_temperature.value
            self.max_temperature = max_temperature.value
            self.temperature_control_is_working = True
            self.current_set_temperature = self.temperature_control_get_temperature()[0]
            return True
        else:
            # The shutter was not closed for some reason
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (temperature_control_init): {msg}")
            return False

    def temperature_control_set_temperature(self, target_temperature):
        if (self.min_temperature is None) or (self.max_temperature is None):
            print("Temperature control was not initialized")
            return False
        status_msg = c_int()
        res = self.library.dll.TemperatureControl_SetTemperature(c_int(target_temperature), byref(status_msg),
                                                                 self.cam_addr)
        if res is True:
            # New target temperature set correctly
            print(f"New target temperature is {target_temperature}")
            self.current_set_temperature = target_temperature
            return True
        else:
            # The shutter was not closed for some reason
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (temperature_control_set_temperature): {msg}")
            return False

    def temperature_control_get_temperature(self):
        """
        Get temperatures of the CCD and back of the termoelectric cooling element (TEC)
        """
        temperature_ccd = c_int()
        temperature_tec = c_int()
        status_msg = c_int()
        # Get the CCD temperature first
        res = self.library.dll.TemperatureControl_GetTemperature(c_int(0), byref(temperature_ccd),
                                                                 byref(status_msg), self.cam_addr)
        if res is True:
            # CCD temperature recieved correctly
            # print(f"CCD temperature is {temperature_ccd.value}")
            pass
        else:
            # Temperature was not recieved
            if status_msg.value == 11:
                # The temperature control was turned off due to overheating of the TEC
                print("The temperature control was turned off due to overheating")
                self.temperature_control_switch_off()
                return None, None
            else:
                msg = ge_lib.messages[status_msg.value]
                if msg == "Busy":
                    return self.temperature_ccd_last_measured, self.temperature_tec_last_measured
                else:
                    print(f"Error (temperature_control_get_temperature, ccd): {msg}")
                    return None, None
        # Get the TEC temperature first
        res = self.library.dll.TemperatureControl_GetTemperature(c_int(1), byref(temperature_tec),
                                                                 byref(status_msg), self.cam_addr)
        if res is True:
            # TEC temperature recieved correctly
            # print(f"TEC temperature is {temperature_tec.value}")
            pass
        else:
            # Temperature was not recieved
            if status_msg.value == 11:
                # The temperature control was turned off due to overheating of the TEC
                print("The temperature control was turned off due to overheating")
                self.temperature_control_switch_off()
                return None, None
            else:
                msg = ge_lib.messages[status_msg.value]
                print(f"Error (temperature_control_get_temperature, tec): {msg}")
                return None, None
        self.temperature_ccd_last_measured = temperature_ccd.value
        self.temperature_tec_last_measured = temperature_tec.value
        return temperature_ccd.value, temperature_tec.value

    def temperature_control_switch_off(self):
        """ Turn off the temperature control """
        status_msg = c_int()
        res = self.library.dll.TemperatureControl_SwitchOff(byref(status_msg), self.cam_addr)
        if res is True:
            # Temperature control switched of correctly
            print("Temperature control switched off")
            self.temperature_control_is_working = False
            return True
        else:
            # Function call has failed for some reason
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (temperature_control_switch_off): {msg}")
            return False

    ###
    #  Taking images
    ###

    def grab_light(self, light_exposure_length, emit_signal=False):
        """
        Make a single light photo
          light_exposure_length is in milliseconds
        """
        self.set_exposure(exptime=light_exposure_length, frametype="normal")
        self.current_exposure_length = light_exposure_length
        self.current_exposure_type = "normal"
        self.take_photo()
        return self.fetch_image()

    def grab_dark(self, dark_exposure_length):
        """
        dark_exposure_length is in milliseconds
        """
        self.set_exposure(exptime=dark_exposure_length, frametype="dark")
        self.current_exposure_length = dark_exposure_length
        self.current_exposure_type = "dark"
        self.take_photo()
        return self.fetch_image()

    def set_exposure(self, exptime, frametype):
        """
        Set exposure time in ms
            frametype - 'normal'     - open shutter exposure
                        'dark'       - exposure with shutter closed
        """
        status_msg = c_int()
        res = self.library.dll.SetExposure(c_int(int(exptime)), byref(status_msg), self.cam_addr)
        if res is True:
            # Exposure set correctly
            self.current_frametype = frametype
            self.current_exposure_length = exptime
            print(f"Exposure set to {exptime/1000} s of {frametype}")
            return True
        else:
            # Camera was not initialized
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (set_exposure): {msg}")
            return False

    def open_shutter(self):
        """
        Open the camera shutter manually and keep it opened
        """
        status_msg = c_int()
        res = self.library.dll.OpenShutter(c_int(1), byref(status_msg), self.cam_addr)
        if res is True:
            # Shutter opened successfully
            print("The shutter is opened now")
            return True
        else:
            # The shutter was not opened
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (open_shutter): {msg}")
            return False

    def close_shutter(self):
        """
        Close the camera shutter manually
        """
        status_msg = c_int()
        res = self.library.dll.OpenShutter(c_int(0), byref(status_msg), self.cam_addr)
        if res is True:
            # Shutter closed successfully
            print("The shutter is closed now")
            # Return the shutter into a trigger mode
            res = self.library.dll.OpenShutter(c_int(2), byref(status_msg), self.cam_addr)
            return True
        else:
            # The shutter was not closed for some reason
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (close_shutter): {msg}")
            return False

    def get_image_size(self):
        width = c_int()
        height = c_int()
        bytes_per_pixel = c_int()
        res = self.library.dll.GetImageSize(byref(width), byref(height), byref(bytes_per_pixel), self.cam_addr)
        if res is True:
            return width.value, height.value, bytes_per_pixel.value
        else:
            return None, None, None

    def take_photo(self):
        # Start exposure
        self.start_exposure()
        # Wait for completion
        while True:
            if not self.dll_is_busy():
                break
            time.sleep(0.25)

    def start_exposure(self):
        """
        Start exposure with parameters that were set by set_exposure function
        """
        correct_bias = c_bool(False)  # Do not correct bias
        show_sync = c_bool(False)  # Do not emit TTL output signal during the exposure
        if self.current_frametype == "dark":
            # Do not open shutter during the exposure, to make a dark frame
            show_shutter = c_bool(False)
        elif self.current_frametype == "normal":
            # Open shutter
            show_shutter = c_bool(True)
        else:
            # Wrong frametype
            print(f"Error (start exposure): frame type is {self.current_frametype} but can be only dark/light")
            return False
        trigger_mode = c_bool(False)  # Camera is triggered internally
        status_msg = c_int()
        # Start the exposure
        res = self.library.dll.StartMeasurement_DynBitDepth(correct_bias, show_sync, show_shutter,
                                                            trigger_mode, byref(status_msg), self.cam_addr)
        if res is True:
            # Exposure started
            print("Exposure started")
            return True
        else:
            # Exposure was not started
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (start_exposure): {msg}")
            return False

    def fetch_image(self):
        width, height, bytes_per_pixel = self.get_image_size()
        print("\n\n\n\n")
        print(width, height, bytes_per_pixel)
        print("\n\n\n\n")
        if width is None:
            return None

        if bytes_per_pixel == 2:
            img_array = np.zeros((height, width), dtype=np.uint16)
            img_ptr = img_array.ctypes.data_as(POINTER(c_uint16))
        else:
            img_array = np.zeros((height, width), dtype=np.uint32)
            img_ptr = img_array.ctypes.data_as(POINTER(c_uint32))
        status_msg = c_int()
        res = self.library.dll.GetMeasurementData_DynBitDepth(byref(img_ptr.contents), status_msg, self.cam_addr)
        if res is True:
            print("Image was fetched successfully")
            if bytes_per_pixel == 2:
                return img_array.astype(np.uint16)
            else:
                img_array[img_array >= 65535] = 65535
                return img_array.astype(np.uint16)
        else:
            msg = ge_lib.messages[status_msg.value]
            print(f"Error (fetch_image): {msg}")
            return None

    def cancel_exposure(self):
        """
        Cancels exposure by closing the shutter
        """
        self.focus_running = False
        res = self.library.dll.StopMeasurement(self.cam_addr)
        if res is True:
            print("Exposure was cancelled")
            return True
        else:
            print("Error (cancel_exposure)")
            return False

    def fill_header(self, hdr):
        """
        Function fills some header cards (like temperature, binning etc)
        into the given header
        """
        ccd_temperature, base_temperature = self.temperature_control_get_temperature()
        time_string = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime())
        hdr["BITPIX"] = 16
        hdr["CCD-TEMP"] = (round(ccd_temperature, 2), "CCD cold part temperature")
        hdr["BASETEMP"] = (round(base_temperature, 2), "CCD hot part temperature")
        hdr["EXPTIME"] = (self.current_exposure_length / 1000, "Exposure length in seconds")
        hdr["XBINNING"] = (self.current_binning, "Binning")
        hdr["YBINNING"] = (self.current_binning, "Binning")
        hdr["INSTRUME"] = ('GreatEyes ELSEi 1024 1024 BI MID', "Camera name")
        hdr["TELESCOP"] = ('AZT-8, 70-cm Crimean Reflector', "Telescope name")
        hdr["CAM-MODE"] = (self.current_mode_string.strip(), "Camera readout mode")
        hdr["DATE"] = time_string
        hdr["DATE-OBS"] = (time_string, "Observation date and time")

    def focus_cycle(self, focus_exposure_length, bin_size, image_queue):
        """
        Get a set of open-shutter exposures
        """
        # Remember pre-focusing camera settings so we can revert them
        # when the focusing is finished
        before_fucusing_bin_size = self.current_binning
        before_focusing_freq_mode = self.current_mode_string
        # Setup camera for focusing
        self.set_binning(bin_size)
        self.set_frequency_mode("1_MHz")
        self.set_exposure(exptime=focus_exposure_length, frametype="normal")
        self.focus_running = True
        # Start the focusing cycle
        while self.focus_running is True:
            self.take_photo()
            image = self.fetch_image()
            image_queue.put(image)
            if image is None:
                # Exposition was aborted
                break
        # Revert pre-focusing camera settings
        self.set_binning(before_fucusing_bin_size)
        self.set_frequency_mode(before_focusing_freq_mode)


class FakeCamera:
    """
    A class to replace a real camera. It simulates the real camera calls for the
    testing purposes
    """
    def __init__(self, cam_addr, ether_addr):
        self.cam_addr = cam_addr
        self.ether_addr = ether_addr
        self.busy = False
        self.connecting = False
        self.model_id = None
        self.model_str = None
        self.connected = False
        self.keep_connected = True
        self.current_interface_type = None
        self.min_temperature = None
        self.max_temperature = None
        self.current_set_temperature = None
        self.current_frametype = None
        self.current_mode_string = ""
        self.temperature_control_is_working = False
        self.temperature_ccd_last_measured = None
        self.temperature_tec_last_measured = None
        self.current_binning = 1
        self.readout_timings_bin1 = {"50_kHz": 22.2,
                                     "100_kHz": 11.3,
                                     "250_kHz": 4.75,
                                     "500_kHz": 2.6,
                                     "1_MHz": 1.25,
                                     "3_MHz": 0.5}
        self.readout_timings_bin2 = {"50_kHz": 11.2,
                                     "100_kHz": 5.7,
                                     "250_kHz": 2.4,
                                     "500_kHz": 1.3,
                                     "1_MHz": 0.7,
                                     "3_MHz": 0.3}
        self.readout_timings = {"50_kHz": 22.2,
                                "100_kHz": 11.3,
                                "250_kHz": 4.75,
                                "500_kHz": 2.6,
                                "1_MHz": 1.25,
                                "3_MHz": 0.5}
        self.current_readout_timing = None

    def setup_camera_interface(self, iface_type="USB", ip_address=None):
        return True

    def connect_to_single_camera_server(self):
        return True

    def disconnect_camera_server(self):
        return True

    def connect_camera(self):
        self.model_id = "001"
        self.model_str = "FakeCamera"
        self.connected = True
        self.keep_connected = True
        return True

    def disconnect_camera(self):
        self.model_id = None
        self.model_str = None
        self.connected = False
        self.keep_connected = False
        return True

    def init_camera(self):
        """
        Initialize the camera (once right after connection)
        """
        self.temperature_control_init()
        self.set_frequency_mode("1_MHz")

    def connect_via_ethernet(self):
        """
        Connect to the camera via ethernet: setup, connect to server, connect, initialize
        """
        self.connecting = True
        self.setup_camera_interface("Ethernet", self.ether_addr)
        self.connect_to_single_camera_server()
        self.connect_camera()
        self.init_camera()
        self.setup_output_mode()
        Thread(target=self.pinger).start()
        self.connecting = False
        return True

    def close_connection(self):
        self.disconnect_camera()
        self.disconnect_camera_server()
        self.connected = False

    def pinger(self):
        idx = 0
        while self.connected:
            if idx == 10:
                ping_time = ping3.ping("172.27.77.40", timeout=5)
            else:
                ping_time = ping3.ping("127.0.0.1", timeout=5)
            if (ping_time is None) or (ping_time > 0.01):
                current_date_and_time = datetime.now()
                with open("ping_log.dat", "a") as fout:
                    fout.write(f"{current_date_and_time}  {ping_time}\n")
            time.sleep(2)
            idx += 1

    def dll_is_busy(self):
        return self.busy

    def set_frequency_mode(self, readout_speed):
        self.current_mode_string = readout_speed
        self.current_readout_timing = self.readout_timings[readout_speed]
        return True

    def set_binning(self, binning):
        if not (0 <= binning <= 7):
            print("Binning must be from 0 to 7")
            return False
        self.current_binning = binning
        return True

    def setup_gain(self, gain_setting):
        return True

    def setup_output_mode(self):
        return True

    def temperature_control_init(self):
        self.min_temperature = -40
        self.max_temperature = +50
        self.temperature_control_is_working = True
        self.current_set_temperature = 10
        return True

    def temperature_control_set_temperature(self, target_temperature):
        if (self.min_temperature is None) or (self.max_temperature is None):
            print("Temperature control was not initialized")
            return False
        self.current_set_temperature = target_temperature
        return True

    def temperature_control_get_temperature(self):
        if self.current_set_temperature is None:
            return None, None
        return (self.current_set_temperature, self.current_set_temperature+30)

    def temperature_control_switch_off(self):
        self.temperature_control_is_working = False
        return True

    def grab_light(self, light_exposure_length, emit_signal=False):
        self.set_exposure(exptime=light_exposure_length, frametype="normal")
        self.current_exposure_length = light_exposure_length
        self.current_exposure_type = "normal"
        self.take_photo()
        return self.fetch_image()

    def grab_dark(self, dark_exposure_length):
        self.set_exposure(exptime=dark_exposure_length, frametype="dark")
        self.current_exposure_length = dark_exposure_length
        self.current_exposure_type = "dark"
        self.take_photo()
        return self.fetch_image()

    def set_exposure(self, exptime, frametype):
        self.current_frametype = frametype
        self.current_exposure_length = exptime
        return True

    def open_shutter(self):
        return True

    def close_shutter(self):
        return True

    def get_image_size(self):
        return 1024/2**self.current_binning, 1024/2**self.current_binning, 8

    def take_photo(self):
        # Start exposure
        self.start_exposure()
        time.sleep(0.25)
        # Wait for completion
        while True:
            if not self.dll_is_busy():
                break
            time.sleep(0.25)

    def start_exposure(self):
        self.busy = True
        t_start = time.time()
        self.exposure_canceled = False
        while time.time() - t_start < self.current_exposure_length/1000:
            time.sleep(0.1)
            if self.exposure_canceled:
                self.busy = False
                return False
        self.busy = False
        return True

    def fetch_image(self):
        if self.exposure_canceled:
            print("Sending none")
            return None
        width, height, bytes_per_pixel = self.get_image_size()
        if self.current_frametype == "dark":
            return np.zeros((int(width), int(height)))
        else:
            return np.random.normal(loc=10000, scale=150, size=(int(width), int(height)))

    def cancel_exposure(self):
        """
        Cancels exposure by closing the shutter
        """
        self.focus_running = False
        self.exposure_canceled = True
        return True

    def fill_header(self, hdr):
        ccd_temperature, base_temperature = self.temperature_control_get_temperature()
        time_string = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime())
        hdr["BITPIX"] = 16
        hdr["CCD-TEMP"] = (round(ccd_temperature, 2), "CCD cold part temperature")
        hdr["BASETEMP"] = (round(base_temperature, 2), "CCD hot part temperature")
        hdr["EXPTIME"] = (self.current_exposure_length / 1000, "Exposure length in seconds")
        hdr["XBINNING"] = 2**self.current_binning
        hdr["INSTRUME"] = ('FakeCamera', "Camera name")
        hdr["CAM-MODE"] = (self.current_mode_string, "Camera readout mode")
        hdr["DATE"] = time_string
        hdr["DATE-OBS"] = (time_string, "Observation date and time")

    def focus_cycle(self, focus_exposure_length, bin_size, image_queue):
        """
        Get a set of open-shutter exposures
        """
        # Remember pre-focusing camera settings so we can revert them
        # when the focusing is finished
        before_fucusing_bin_size = self.current_binning
        before_focusing_freq_mode = self.current_mode_string
        # Setup camera for focusing
        self.set_binning(bin_size)
        self.set_frequency_mode("1_MHz")
        self.set_exposure(exptime=focus_exposure_length, frametype="normal")
        self.focus_running = True
        # Start the focusing cycle
        while self.focus_running is True:
            self.take_photo()
            image = self.fetch_image()
            image_queue.put(image)
            if image is None:
                # Exposition was aborted
                break
        # Revert pre-focusing camera settings
        self.set_binning(before_fucusing_bin_size)
        self.set_frequency_mode(before_focusing_freq_mode)


if __name__ == "__main__":
    cam = GECamera(cam_addr=0)
    cam.connect_via_ethernet("192.168.1.234")
    cam.close_connection()
