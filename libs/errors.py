#! /usr/bin/env python


class DeviceIsNotAvailableError(Exception):
    pass
