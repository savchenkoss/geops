#! /usr/bin/env python

from os import path


class ConfigParams(object):
    def __init__(self):
        # Load config parameters
        self.path_to_config = path.join(path.dirname(__file__), "config.dat")
        self.params = {}
        if not path.exists(self.path_to_config):
            # If there is no file, use these default values
            self.params["rotator_zero_point"] = 0.0
            self.params["wheel_1_shift"] = 0
            self.params["wheel_2_shift"] = 0
            self.params["path_to_save_files"] = "."
            fout = open(self.path_to_config, "w")
            for param_name, param_value in self.params.items():
                fout.write(f"{param_name} {param_value}\n")
            fout.close()

        for line in open(self.path_to_config):
            param_name, param_value = line.split()[:2]
            self.params[param_name] = param_value

    def update_parameter(self, param_name, new_value):
        if param_name not in self.params:
            return
        if new_value != self.params[param_name]:
            self.params[param_name] = new_value
            fout = open(self.path_to_config, "w")
            for param_name, param_value in self.params.items():
                fout.write(f"{param_name} {param_value}\n")
            fout.close()
