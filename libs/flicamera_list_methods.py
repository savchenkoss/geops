    def __init__(self, dev_name, model):

    def set_camera_mode(self, mode):
        """ Readout freq goes here """

    def get_camera_mode(self):
        """ Returns readout frequency """

    def get_image_size(self):
        "returns (row_width, img_rows, img_size)"

    def set_image_binning(self, hbin=1, vbin=1):

    def set_temperature(self, T):
        "set the camera's temperature target in degrees Celcius"

    def get_temperature(self):
        "gets the camera's temperature in degrees Celcius"

    def read_CCD_temperature(self):
        "gets the CCD's temperature in degrees Celcius"

    def read_base_temperature(self):
        "gets the cooler's hot side in degrees Celcius"

    def get_cooler_power(self):
        "gets the cooler's power in watts (undocumented API function)"

    def open_shutter(self):
        "opens the camera's shutter"

    def close_shutter(self):
        "closes the camera's shutter"

    def set_exposure(self, exptime, frametype="normal"):
        """setup the exposure type:
               exptime   - exposure time in milliseconds
               frametype -  'normal'     - open shutter exposure
                            'dark'       - exposure with shutter closed
        """

    def take_photo(self, wait_shutter=True, camera_status_value=None, emit_signal=False):
        """
        Expose the frame, wait for completion, and fetch the image data.
        """

    def start_exposure(self):
        """ Begin the exposure and return immediately.
            Use the method  'get_timeleft' to check the exposure progress
            until it returns 0, then use method 'fetch_image' to fetch the image
            data as a numpy array.
        """

    def cancel_exposure(self):
        """
        Cancels exposure by closing the shutter
        """

    def get_exposure_timeleft(self):
        """ Returns the time left on the exposure in milliseconds.
        """

    def fetch_image(self, wait_shutter=True, emit_signal=False):
        """
        Fetch the image data for the last exposure. Returns a numpy.ndarray object.
        """
