#! /usr/bin/env python

import zmq


class Signal(object):
    def __init__(self):
        self.port = "5556"
        self.context = zmq.Context()

    def send_signal(self, msg):
        self.socket = self.context.socket(zmq.PAIR)
        self.socket.setsockopt(zmq.SNDTIMEO, 1000)
        self.socket.bind("tcp://*:%s" % self.port)
        print("Sending signal")
        message = "%s" % str(msg)
        try:
            self.socket.send(message.encode())
            print("Signal sent")
        except zmq.error.Again:
            print("not connected")
        self.socket.close()
