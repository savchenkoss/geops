#! /usr/bin/env python

import socket
import threading
import json
import time


class FakeWindow(object):
    def __init__(self):
        self.alive = True


class AZT_Onboard(object):
    def __init__(self, window, ip_address, port):
        self.window = window
        self.ip_address = ip_address
        self.port = port
        self.is_running = False  # True if this object operates
        self.active_connection = False  # True if there is an active connection to the unboard server
        self.keep_alive = True  # Client will automatically try to reconnect while True
        self.state = {}
        # What to say if there is no connection
        self.default_state = {'focuser': {'Connected': None, 'Position': None, 'Temperature': None, 'Moving': None},
                              'rotator': {'Connected': None, 'Posang': None, 'Moving': None},
                              'wheel_1': {'Exists': None, 'Connected': None, 'Filter_pos': None, 'Moving': None},
                              'wheel_2': {'Exists': None, 'Connected': None, 'Filter_pos': None, 'Moving': None}}

    def connect(self):
        while self.window.alive:
            try:
                self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.s.connect((self.ip_address, self.port))
                self.s.settimeout(0.05)
                self.is_running = True
                self.active_connection = True
                break
            except socket.error:
                time.sleep(1)
                continue
        threading.Thread(target=self.listen).start()

    def listen(self):
        while (self.is_running and self.active_connection):
            # print(self.state)
            try:
                data = self.s.recv(2048).decode()
                if data:
                    try:
                        self.state = json.loads(data)
                    except ValueError:
                        pass
                else:
                    # If we got here, the server is, probably, disconnected
                    self.is_running = False
                    if self.keep_alive:
                        threading.Thread(target=self.connect).start()
            except socket.timeout:
                pass
            except ConnectionResetError:
                # Closed connection
                self.is_running = False
                if self.keep_alive:
                    threading.Thread(target=self.connect).start()
        self.s.close()
        self.state = self.default_state

    def close(self):
        self.send_command("server", "shutdown", ())
        time.sleep(2)
        self.keep_alive = False
        self.is_running = False
        self.active_connection = False

    def detatch(self):
        """
        Close connection to the server, but kiip the server running and waiting for
        a new connection to continue the work
        """
        self.keep_alive = False  # To stop automatical reconnection
        self.active_connection = False  # To stop the main loop in listen() function

    def attach(self):
        """
        Connect to the server which is running already
        """
        self.keep_alive = True
        threading.Thread(target=self.connect).start()

    def send_command(self, device, command, parameters):
        if not self.is_running:
            return
        data = {"device": device, "command": command, "parameters": parameters,
                "send_back": True}
        msg = json.dumps(data) + "\n"
        # print("Sending command:", msg)
        try:
            self.s.send(msg.encode())
        except (BrokenPipeError, OSError):
            # If we got here, the server is probably disconnected
            self.is_running = False
            if self.keep_alive:
                threading.Thread(target=self.connect).start()


if __name__ == "__main__":
    window = FakeWindow()
    azt = AZT_Onboard(window, "127.0.0.1", 6546)
    print("Status:", azt.state)
    print("Connecting...")
    azt.connect()
    print("Connected")
    time.sleep(2)
    print("Detaching")
    azt.detatch()
    time.sleep(1)
    print("Attaching")
    azt.attach()
    time.sleep(2)
    print("Detaching")
    azt.detatch()
    time.sleep(1)
    print("Attaching")
    azt.attach()
    time.sleep(1)
    print("Closing...")
    azt.close()
    print("Status:", azt.state)
