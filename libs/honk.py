#! /usr/bin/env python

import os
from threading import Thread


if os.name == "nt":
    import winsound


def honk(freq, duration):
    """
    Produces a sound. Only works on NT
    """
    if os.name == 'nt':
        Thread(target=winsound.Beep, args=(freq, duration)).start()
    else:
        print("Beep!")
