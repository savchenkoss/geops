#! /usr/bin/env python

import os
from ctypes import byref, c_long
import time
from threading import Thread
from .fli_lib import FLILibrary, flidomain_t, FLIDOMAIN_USB, FLIDEVICE_FILTERWHEEL
from .fli_device import USBDevice


class USBFilterWheel(USBDevice):
    # load the DLL
    _libfli = FLILibrary.getDll(debug=False)
    _domain = flidomain_t(FLIDOMAIN_USB | FLIDEVICE_FILTERWHEEL)

    def __init__(self, dev_name, model):
        USBDevice.__init__(self, dev_name=dev_name, model=model)

    def set_filter_pos(self, pos):
        self._libfli.FLISetFilterPos(self._dev, c_long(pos))

    def get_filter_pos(self):
        pos = c_long()
        self._libfli.FLIGetFilterPos(self._dev, byref(pos))
        return pos.value

    def get_filter_count(self):
        count = c_long()
        self._libfli.FLIGetFilterCount(self._dev, byref(count))
        return count.value

    def step_motor(self, step):
        step = c_long(int(step))
        self._libfli.FLIStepMotor(self._dev, step)
        return


class FakeWheel(object):
    def __init__(self, filters):
        self.filters = filters
        self.current_filter_name = "Empty"
        self.current_idx = 7

    def set_filter(self, filter_name):
        self.current_filter_name = filter_name

    def roll_filters(self):
        pass

    def get_filter_pos(self):
        return self.current_idx

    def set_filter_pos(self, pos):
        time.sleep(1)
        self.current_idx = pos

    def step_motor(self, step):
        return


class FLIWheel(object):
    def __init__(self, server, filters, serial_number, bias=0, debug=False):
        """
        Creates an instance of a FLI filterwheel devise.
        filters: a dictionary with filter name -- wheel position pairs.
        default_filter : The filter, which will be set during the device initialization.
        serial_number: a devise serial number to pick.
        bias: int, zero point shift of the wheel.
        """
        self.server = server
        self.filters = filters
        self.bias = bias
        self.num_of_filters = len(self.filters)
        self.default_filter = list(self.filters.keys())[0]
        self.current_filter_name = ""
        self.is_alive = True
        self.is_ready = False
        self.moving = False
        self.roll_filters()  # Select first and then empty slot

    def roll_filters(self):
        def f():
            for filter_name in self.filters.keys():
                self.set_filter(filter_name)
            self.set_filter(self.default_filter)
            self.step_motor(self.bias)  # Zero point shift
            self.is_ready = True
        Thread(target=f).start()

    def shutdown(self):
        def f():
            self.server.send_command("wheel_1").set_filter_pos(0)
            self.is_alive = False
        Thread(target=f).start()

    def callback(self, filter_name):
        if self.moving is False:
            thread = Thread(target=self.set_filter, args=(filter_name, ))
            thread.start()

    def set_filter(self, filter_name):
        filter_index = self.filters[filter_name]
        current_filter_index = self.device.get_filter_pos()
        if filter_index == current_filter_index:
            # The chosen filter is already selected
            return
        print("setting %s" % filter_name)
        self.moving = True

        # Set filter position
        self.device.set_filter_pos(filter_index)
        # Get filter position to ensure success
        new_filter_index = self.device.get_filter_pos()
        print(filter_name, filter_index, new_filter_index)
        self.moving = False
        self.current_filter_name = filter_name

    def step_motor(self, step):
        self.device.step_motor(step)
