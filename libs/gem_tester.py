#! /usr/bin/env python

from focuser_rotator import GeminiFocuserRotator

gfr = GeminiFocuserRotator()
print(gfr.get_focuser_status())
print(gfr.get_rotator_status())
print(gfr.focuser_halt())
print(gfr.rotator_halt())
gfr.shutdown()
